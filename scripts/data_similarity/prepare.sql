-- 查看建表语句
SELECT sql FROM sqlite_master WHERE type='table' AND name = 'spikes';

-- 创建spikes表
CREATE TABLE spikes (
    id TEXT,
    code TEXT,
    date TEXT,
    type TEXT,
    direction TEXT,
    value REAL
)

-- 创建terms表
CREATE TABLE terms (
    name TEXT,
    code TEXT,
    type TEXT,
    spike_id TEXT,
    spike_date TEXT,
    spike_direction TEXT,
    value REAL
)