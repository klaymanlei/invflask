#coding:utf-8

from data_processor import gen_mn, find_spikes, split_part

def load(file):
    rs = []
    for f in open(file, 'r'):
        data = f.split(',')
        rs.append(data)
    rs.reverse()
    return rs


def update_part(parts, spike):
    last_part = parts[-1]
    if len(last_part[1]) < 3:
        last_part[1].append((spike[0], round(spike[1], 2)))
    elif len(last_part[1]) == 3:
        last_part[1].append((spike[0], round(spike[1], 2)))
        last_direct = last_part[1][-2][1] - last_part[1][-4][1]
        cur_direct = last_part[1][-1][1] - last_part[1][-3][1]
        if last_direct * cur_direct <= 0:
            new_part = (0, last_part[1])
        elif last_direct > 0 and cur_direct > 0:
            new_part = (1, last_part[1])
        else:
            new_part = (-1, last_part[1])
        parts.pop()
        parts.append(new_part)
    else:
        last_direct = last_part[1][-1][1] - last_part[1][-3][1]
        cur_direct = spike[1] - last_part[1][-2][1]
        if last_part[0] == 0:
            if last_direct * cur_direct <= 0:
                last_part[1].append((spike[0], round(spike[1], 2)))
                return
            else:
                new_part = last_part[1][-2:]
                new_part.append((spike[0], round(spike[1], 2)))
                parts.append((0, new_part))
        else:
            if last_direct * cur_direct > 0:
                last_part[1].append((spike[0], round(spike[1], 2)))
                return
            else:
                new_part = last_part[1][-2:]
                new_part.append((spike[0], round(spike[1], 2)))
                parts.append((0, new_part))


if __name__ == '__main__':
    file = '../data/data_similarity/my01.csv'
    start = '2016/2/29'
    data = load(file)
    n = 5

    t_data = []
    for rec in data:
        if rec[1] == start:
            break
        t_data.append((rec[1], float(rec[2])))
    mn = gen_mn(t_data, n)

    spikes = find_spikes(mn)

    parts = split_part(spikes)
    last_part = parts[-1]
    allin = False
    if last_part[0] == 1 and len(last_part[1]) > 4:
        allin = True
    elif last_part[0] == 1 and len(last_part[1]) == 4 and last_part[1][-1][1] < last_part[1][-2][1]:
        allin = True
    print allin

    last2_avg = ()
    last_avg = ()
    last_rec = []
    sim = False
    out_ready = False
    in_ready = False
    in_next_spike = False
    for rec in data:
        if rec[2] == '0':
            continue

        # if rec[1] == '2016/4/19':
        #     print rec[1]

        if out_ready:
            allin = False
            out_ready = False
            print '\t%s\t\t%s' % (rec[1], rec[5])
        elif in_ready:
            allin = True
            in_ready = False
            print '%s\t\t%s\t' % (rec[1], rec[5])

        last_rec.append(rec)
        if rec[1] == start:
            sim = True

        avg = 0
        for r in last_rec:
            avg += float(r[2])
        avg = round(avg / n, 2)

        if sim:
            is_spike = (last_avg[1] > last2_avg[1] and last_avg[1] > avg) or (last_avg[1] < last2_avg[1] and last_avg[1] < avg)
            if is_spike:
                update_part(parts, last_avg)
                last_part = parts[-1]
                if in_next_spike and last_part[0] == 1:
                    in_next_spike = False
                    in_ready = True
                elif allin and last_part[0] != 1:
                    out_ready = True
                elif allin and last_part[0] == 1 and last_part[1][-1][1] < last_part[1][-2][1]:
                    in_ready = True
                elif not allin and len(last_part[1]) == 4 and last_part[0] == 1:
                        sub_spikes = last_part[1]
                        if sub_spikes[-1][1] < sub_spikes[-2][1]:
                            in_ready = True
                        elif sub_spikes[-1][1] > sub_spikes[-2][1]:
                            in_next_spike = True
            elif allin:
                last_part = parts[-1]
                if avg <= min(last_part[1][-2][1], last_part[1][-1][1]):
                    out_ready = True

        if last_avg == () or avg != last_avg[1]:
            last2_avg = last_avg
        last_avg = (rec[1], avg)

        if len(last_rec) >= n:
            last_rec.pop(0)

    for part in parts:
        print part[0], part[1][0], part[1][-1], part[1]
