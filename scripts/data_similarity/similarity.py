#coding:utf-8

import random
import numpy as np
from fakedata import fake
import time
from utils import is_number


# 计算欧式距离
def euclidean_distance(vector1, vector2):
    sum = 0
    for i in range(len(vector1)):
        sum += float(pow(vector1[i] * vector2[i] > 0 and (vector1[i] - vector2[i]) / 2 or (vector1[i] - vector2[i]), 2))
    return pow(sum, 0.5)

# 计算余弦相似度
def cosine_similarity(vector1, vector2):
    dot_product = 0.0
    normA = 0.0
    normB = 0.0
    for a, b in zip(vector1, vector2):
        dot_product += a * b
        normA += a ** 2
        normB += b ** 2
    if normA == 0.0 or normB == 0.0:
        return 0
    else:
        return round(dot_product / ((normA**0.5)*(normB**0.5)) * 100, 2)


if __name__ == '__main__':
    n = 60  # 每组数据个数
    values_format = {}
    showdatas = {}

    # ===== 生成随机数据用于测试 =====
    # values = {}
    # for i in range(4000):
    #     code = r's%03d' % i
    #     rate = np.random.normal(loc=0, scale=0.003, size=None) + 0.000166667
    #     start = round(random.random() * 100, 2)
    #     # print code, start, rate, start * pow(1 + rate, n)
    #     # if not round(rate, 3) in test:
    #     #     test[round(rate, 3)] = 0
    #     # test[round(rate, 3)] += 1
    #     value = fake(start, start * pow(1 + rate, n), n)
    #     values[code] = value
    #
    # for code in values:
    #     value = values[code]
    #     if 0 in value:
    #        continue
    #     value_format = []
    #     for i in range(1, n):
    #         f = float(value[i] / value[i - 1] - 1)
    #         value_format.append(f)
    #     values_format[code] = value_format
    # ===== 生成随机数据用于测试 =====

    # ===== 从文件中读取数据 =====
    files = ('../data/data_similarity/my01.csv', '../data/data_similarity/ws01.csv')
    for file in files:
        f = open(file, 'r')
        chgs = []
        ps = []
        for line in f:
            chg = line.split(',')[8]
            date = line.split(',')[1]
            code = line.split(',')[0]
            showdata = line.split(',')[2]

            chgs.insert(0, float(is_number(chg) and chg or 0) / 100)
            ps.insert(0, float(is_number(showdata) and showdata or 0))
            while len(chgs) > n:
                chgs.pop()
                ps.pop()
            if len(chgs) == n:
                chgs_tmp = []
                ps_tmp = []
                chgs_tmp.extend(chgs)
                ps_tmp.extend(ps)
                values_format['%s_%s' % (code, date)] = chgs_tmp
                showdatas['%s_%s' % (code, date)] = ps_tmp
    # ===== 从文件中读取数据 =====

    # ===== 给定向量计算最相似向量 =====
    # cache_values = []
    # cache_code = ''
    # for code in values_format:
    #     if len(cache_values) == 0:
    #         cache_values = values_format[code]
    #         cache_code = code
    #         continue
    #     comp_value = values_format[code]
    #     sim = True
    #     for i, j in ((0, 19), (20, 39), (40, 58)):
    #         diff = euclidean_distance(cache_values[i:j], comp_value[i:j])
    #         # print diff
    #         # if diff <= 0.3:
    #         #     print values[cache_code][i:j]
    #         #     print values[code][i:j]
    #         #     print code, diff
    #         if diff > 0.15 or diff < -0.15:
    #             sim = False
    #             break
    #     if sim and (values[code][0] - values[code][-1]) * (values[cache_code][0] - values[cache_code][-1]) > 0:
    #         print values[cache_code]
    #         print values[code]
    #         print euclidean_distance(cache_values, comp_value)
    # ===== 给定向量计算最相似向量 =====

    # 计算所有向量两两距离
    keys = values_format.keys()
    ts = time.time()
    print ts
    cnt = 0
    test = {}
    sims = []
    for i in range(0, len(keys)):
        code_a = keys[i]
        for j in range(i + 1, len(keys)):
            code_b = keys[j]
            if code_a[5:] == code_b[5:]:
                continue
            diff = euclidean_distance(values_format[code_a], values_format[code_b])
            if diff < 0.07:
                print code_a, code_b
                print showdatas[code_a]
                print showdatas[code_b]
                print diff
            if not round(diff, 1) in test:
                test[round(diff, 1)] = 0
            test[round(diff, 1)] += 1
            cnt += 1
            if cnt % 100000 == 0:
                print cnt
    te = time.time()
    print te - ts
    print test