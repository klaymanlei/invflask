# coding:utf-8

import time
import logging

from scripts.data_similarity.gin.Seed_Bag import Seed_Bag
from scripts.data_similarity.gin.node_factory import *

class St_6_to_io(object):

    def __init__(self):
        pass

    def score(self, spikes, node1, node2, hist):
        status = False
        params = []
        s = 1
        p = -1
        in_cnt = 0
        vic_cnt = 0
        for spike in spikes:
            if spike.direction != '0':
                d = time.strptime(spike.date, '%Y-%m-%d')
                params.append(time.mktime(d) / 10000)
                params.append(spike.value)
                if len(params) == 12:
                    if not status:
                        rs = node1.evaluate(params)
                        if rs > 0:
                            in_cnt += 1
                            status = True
                            d_day_1 = hist[spike.date][1]
                            d_day_2 = hist[d_day_1][1]
                            logging.debug('i %s %f' % (d_day_2, hist[d_day_2][0].col1))
                            p = hist[d_day_2][0].col1
                    else:
                        rs = node2.evaluate(params)
                        if rs > 0:
                            status = False
                            d_day_1 = hist[spike.date][1]
                            d_day_2 = hist[d_day_1][1]
                            logging.debug('o %s %f' % (d_day_2, hist[d_day_2][0].col1))
                            s *= hist[d_day_2][0].col1 / p
                            if hist[d_day_2][0].col1 > p:
                                vic_cnt += 1
                    params = params[2:]
        return s, in_cnt, vic_cnt

    def one_lap(self, nodes, spikes, hist):
        seeds = Seed_Bag()
        start = time.time()
        score_max = []
        score_max_note = ''
        vic_max = []
        vic_max_note = ''
        vic_min = []
        vic_min_note = ''

        score_max_2 = []
        vic_max_2 = []

        for i in range(0, len(nodes)):
            logging.debug('%s%%' % ((i + 1) / len(nodes) * 100))
            for j in range(0, len(nodes)):
                node1 = nodes[i]
                node2 = nodes[j]
                (s, in_cnt, vic_cnt) = self.score(spikes, node1, node2, hist)
                if in_cnt == 0:
                    continue
                vic_rate = float(vic_cnt)/in_cnt

                if in_cnt < 2:
                    continue

                if len(score_max) == 0:
                    score_max.append(s)
                    score_max.append((node1, node2))
                    score_max_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)
                    score_max_2.append(s)
                    score_max_2.append((node1, node2))
                else:
                    if s > score_max[0] or (s == score_max[0] and (node1.length() + node2.length()) < (score_max[1][0].length() + score_max[1][1].length())):
                        score_max_2[0] = score_max[0]
                        score_max_2[1] = score_max[1]
                        score_max[0] = s
                        score_max[1] = (node1, node2)
                        score_max_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)
                    elif s > score_max_2[0] or (s == score_max_2[0] and (node1.length() + node2.length()) < (score_max_2[1][0].length() + score_max_2[1][1].length())):
                        score_max_2[0] = s
                        score_max_2[1] = (node1, node2)


                if len(vic_max) == 0:
                    vic_max.append(vic_rate)
                    vic_max.append((node1, node2))
                    vic_max_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)
                    vic_max_2.append(vic_rate)
                    vic_max_2.append((node1, node2))
                else:
                    if in_cnt > 10 and (vic_rate > vic_max[0] or (vic_rate == vic_max[0] and (node1.length() + node2.length()) < (vic_max[1][0].length() + vic_max[1][1].length()))):
                        vic_max_2[0] = vic_max[0]
                        vic_max_2[1] = vic_max[1]
                        vic_max[0] = vic_rate
                        vic_max[1] = (node1, node2)
                        vic_max_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)
                    elif in_cnt > 10 and (vic_rate > vic_max_2[0] or (vic_rate == vic_max_2[0] and (node1.length() + node2.length()) < (vic_max_2[1][0].length() + vic_max_2[1][1].length()))):
                        vic_max_2[0] = vic_rate
                        vic_max_2[1] = (node1, node2)

                if len(vic_min) == 0:
                    vic_min.append(vic_rate)
                    vic_min.append((node1, node2))
                    vic_min_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)
                elif in_cnt > 10 and (vic_rate < vic_min[0] or (vic_rate == vic_min[0] and (node1.length() + node2.length()) < (vic_min[1][0].length() + vic_min[1][1].length()))):
                    vic_min[0] = vic_rate
                    vic_min[1] = (node1, node2)
                    vic_min_note = 'score: %f, in_cnt: %s, vic_rate: %f' % (s, in_cnt, vic_rate)

        logging.info('evaluate')

        seeds.put_in(score_max[1][0], 'score max')
        seeds.put_in(score_max[1][1], 'score max')
        seeds.put_in(score_max_2[1][0], 'score max')
        seeds.put_in(score_max_2[1][1], 'score max')
        seeds.note('score max', score_max_note)

        seeds.put_in(vic_max[1][0], 'vic max')
        seeds.put_in(vic_max[1][1], 'vic max')
        seeds.put_in(vic_max_2[1][0], 'vic max')
        seeds.put_in(vic_max_2[1][1], 'vic max')
        seeds.note('vic max', vic_max_note)

        seeds.put_in(vic_min[1][0], 'vic min')
        seeds.put_in(vic_min[1][1], 'vic min')
        seeds.note('vic min', vic_min_note)

        end = time.time()
        logging.info('from %f to %f, spend %f' % (start, end, end - start))
        # logging.info('there are %s winners' % (len(winners)))
        return seeds
