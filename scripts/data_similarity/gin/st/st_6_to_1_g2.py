# coding:utf-8

import logging
import time

from scripts.data_similarity.gin.Seed_Bag import Seed_Bag
from scripts.data_similarity.gin.node_factory import *
from st_abstract import St_Abstract


class St_6_to_1_g2(St_Abstract):

    def __init__(self, seed_cnt=10):
        St_Abstract.__init__(self, seed_cnt)

    def score(self, evals, answers, avg):
        cnt = 0
        s = 0
        for i in range(0, len(evals)):
            if evals[i][1] is None or evals[i][1] == 0:
                continue
            cnt += 1

            if abs(answers[evals[i][0]]) > avg * 2:
                if evals[i][1] * answers[evals[i][0]] > 0:
                    s += 4
                else:
                    s -= 4
            elif abs(answers[evals[i][0]]) < avg / 2:
                if evals[i][1] * answers[evals[i][0]] > 0:
                    s += 1
            else:
                if evals[i][1] * answers[evals[i][0]] > 0:
                    s += 2
                else:
                    s -= 1
        if cnt < 5:
            return (-1, -1)
        else:
            return (float(s), cnt)

    def one_lap(self, nodes, spikes, hist):
        start = time.time()
        seeds = Seed_Bag()
        score_key = {}
        vic_key = {}
        total = 0

        answers, avg = self.get_answers(spikes, hist)
        # answers = {}
        # last_p = -1
        # sum = 0
        # for spike in spikes:
        #     if spike.direction != '0':
        #         total += 1
        #
        #         day_1 = hist[spike.date][1]
        #         day_2 = hist[day_1][1]
        #         p = hist[day_2][0].col1
        #         if last_p > 0:
        #             answers[spike.date] = p - last_p
        #             sum += abs(answers[spike.date])
        #         last_p = p
        #
        # avg = sum / len(answers)

        for i in range(0, len(nodes)):
            node = nodes[i]
            evals = self.evaluate(spikes, node)
            (total_s, cnt) = self.score(evals, answers, avg)
            # s = (float(total_s) / cnt) * 0.7 + (float(cnt) / total) * 0.3
            s = total_s
            if s in score_key:
                prev = score_key[s]
                if total_s > prev[1]:
                    score_key[s] = (node, total_s)
                elif node.length() < prev[0].length():
                    score_key[s] = (node, total_s)
            else:
                score_key[s] = (node, total_s)
        logging.debug('evaluate')

        scores = score_key.keys()
        scores.sort(reverse=True)
        # for s in scores:
        #     print s
        for i in range(0, len(nodes) / 3):
            if i > (len(scores) - 1) or scores[i] < 10:
                break
            score = scores[i]
            seeds.put_in(score_key[score][0], 'best score')
        seeds.note('best score', scores[0])

        end = time.time()
        logging.debug('from %f to %f, spend %f' % (start, end, end - start))
        return seeds
