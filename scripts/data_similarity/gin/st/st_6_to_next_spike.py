# coding:utf-8

import logging
import time

from scripts.data_similarity.gin.Seed_Bag import Seed_Bag
from scripts.data_similarity.gin.node_factory import *
from st_abstract import St_Abstract

class St_6_to_Next_Spike(St_Abstract):

    def __init__(self, seed_cnt=10):
        St_Abstract.__init__(self, seed_cnt)

    def score(self, evals, answers, avg):
        cnt = 0
        s = 0
        vic = 0
        for i in range(0, len(evals)):
            if evals[i][1] is None:
                continue
            cnt += 1
            s += abs(evals[i][1] - answers[evals[i][0]])
            if evals[i][1] * answers[evals[i][0]] > 0:
                vic += 1
        if cnt < 5:
            return -1, -1, -1
        else:
            return float(s), cnt, vic

    def one_lap(self, nodes, samples, answers):
        start = time.time()
        seeds = Seed_Bag()
        score_key = {}
        rate_key = {}
        # total = 0

        # answers, avg = self.get_answers(spikes, hist)
        # last_p = -1
        # sum = 0

        # for spike in spikes:
        #     if spike.direction != '0':
        #         total += 1
        #
        #         day_1 = hist[spike.date][1]
        #         day_2 = hist[day_1][1]
        #         p = hist[day_2][0].col1
        #         if last_p > 0:
        #             answers[spike.date] = p - last_p
        #             sum += abs(answers[spike.date])
        #         last_p = p

        # avg = sum / len(answers)

        for i in range(0, len(nodes)):
            node = nodes[i]
            evals = self.evaluate(samples, node)
            total_s, cnt, vic = self.score(evals, answers, 0)
            if total_s == -1 and cnt == -1 and vic == -1:
                continue
            cnt_p = float(cnt) / len(answers) * 0.05
            vic_p = float(vic) / len(answers) * 0.95
            s = cnt_p + vic_p
            if s in score_key:
                prev = score_key[s]
                if vic > prev[2]:
                    score_key[s] = (node, cnt, vic)
                elif vic == prev[2] and node.length() < prev[0].length():
                    score_key[s] = (node, cnt, vic)
            else:
                score_key[s] = (node, cnt, vic)
            m = float(vic) / cnt
            if m > 1:
                print m
            if m in rate_key:
                prev = rate_key[m]
                if cnt > prev[1]:
                    rate_key[m] = (node, cnt)
                elif cnt == prev[1] and node.length() < prev[0].length():
                    rate_key[m] = (node, cnt)
            else:
                rate_key[m] = (node, cnt)
        logging.debug('evaluate')

        scores = score_key.keys()
        scores.sort(reverse=True)
        for i in range(0, self.seed_cnt):
            if i > (len(scores) - 1):
                break
            score = scores[i]
            seeds.put_in(score_key[score][0], 'best score')
        seeds.note('best score', '%f (cnt: %d, vic: %d, champion length: %d)' % (scores[0], score_key[scores[0]][1], score_key[scores[0]][2], score_key[scores[0]][0].length()))

        all_vic = rate_key.keys()
        all_vic.sort(reverse=True)
        for i in range(0, self.seed_cnt):
            if i > (len(rate_key) - 1):
                break
            v = all_vic[i]
            seeds.put_in(rate_key[v][0], 'most vic')
        seeds.note('most vic', '%f (cnt: %d, champion length: %d)' % (all_vic[0], rate_key[all_vic[0]][1], rate_key[all_vic[0]][0].length()))

        end = time.time()
        logging.debug('from %f to %f, spend %f' % (start, end, end - start))
        return seeds
