# coding:utf-8

import logging
import time

from scripts.data_similarity.gin.Seed_Bag import Seed_Bag
from scripts.data_similarity.gin.node_factory import *


class St_Abstract(object):

    def __init__(self, seed_cnt=10):
        self.seed_cnt = seed_cnt

    def get_answers(self, spikes, hist):
        total = 0
        answers = {}
        last_p = -1
        sum = 0
        for spike in spikes:
            if spike.direction != '0':
                total += 1

                day_1 = hist[spike.date][1]
                day_2 = hist[day_1][1]
                p = hist[day_2][0].col1
                if last_p > 0:
                    answers[spike.date] = p - last_p
                    sum += abs(answers[spike.date])
                last_p = p
        avg = sum / len(answers)
        return answers, avg

    def evaluate(self, samples, node):
        scores = []
        for s_date in samples:
            rs = node.evaluate(samples[s_date])
            scores.append((s_date, rs))
        return scores

    def score(self, evals, answers, avg):
        return -1, -1

    def one_lap(self, nodes, spikes, hist):
        seeds = Seed_Bag()
        return seeds
