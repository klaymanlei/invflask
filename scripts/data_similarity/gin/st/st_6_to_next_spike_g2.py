# coding:utf-8

import logging
import time
import math

from scripts.data_similarity.gin.Seed_Bag import Seed_Bag
from scripts.data_similarity.gin.node_factory import *
from st_abstract import St_Abstract

class St_6_to_Next_Spike_g2(St_Abstract):

    def __init__(self, seed_cnt=20):
        St_Abstract.__init__(self, seed_cnt)

    def score(self, evals, answers, avg):
        cnt = 0
        vic = 0
        for i in range(0, len(evals)):
            if evals[i][1] is None:
                continue
            cnt += 1
            if evals[i][1] * answers[evals[i][0]] > 0:
                vic += 1
        if cnt < 5:
            logging.debug('%d, %d' % (-1, -1))
            return -1, -1
        else:
            logging.debug('%d, %d' % (cnt, vic))
            return cnt, vic

    def one_lap(self, nodes, samples, answers):
        start = time.time()
        seeds = Seed_Bag()
        score_key = {}
        rate_key = {}

        for i in range(0, len(nodes)):
            node = nodes[i]
            evals = self.evaluate(samples, node)
            cnt, vic = self.score(evals, answers, 0)
            if cnt == -1 and vic == -1:
                continue
            s = score_coverage(cnt, vic, node.length(), len(answers))
            score_key[s] = (node, cnt, vic)

            # m = score_winning_percentage(cnt, vic, node.length(), len(answers))
            # rate_key[m] = (node, cnt, vic)

        logging.debug('evaluate')

        scores = score_key.keys()
        scores.sort(reverse=True)
        for i in range(0, self.seed_cnt):
            if i > (len(scores) - 1):
                break
            score = scores[i]
            seeds.put_in(score_key[score][0], 'best score')
        seeds.note('best score', '%f (cnt: %d, vic: %d, champion length: %d)' % (scores[0], score_key[scores[0]][1], score_key[scores[0]][2], score_key[scores[0]][0].length()))

        end = time.time()
        logging.debug('from %f to %f, spend %f' % (start, end, end - start))
        return seeds


def score_coverage(cnt, vic, node_len, answer_len):
    coverage_score = float(cnt) / answer_len
    winning_score = float(vic) / cnt
    length_score = 1 / math.log(node_len, 2)
    if winning_score <= 0.8:
        return winning_score * 0.7 + length_score * 0.01 + coverage_score * 0.29
    else:
        return 0.8 * 0.7 + length_score * 0.01 + coverage_score * 0.29


def score_winning_percentage(cnt, vic, node_len, answer_len):
    coverage_score = float(cnt) / answer_len
    winning_score = float(vic) / cnt
    length_score = 1 / math.log(node_len, 2)
    if coverage_score <= 0.3:
        return winning_score * 0.7 + length_score * 0.01 + coverage_score * 0.29
    else:
        return winning_score * 0.7 + length_score * 0.01 + 0.3 * 0.29

