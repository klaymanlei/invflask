# coding:utf-8

from run_gin import *
from node_factory import *
from scripts.data_similarity.gin.st.st_6_to_io import *
from scripts.data_similarity.gin.st.st_6_to_next_spike_g2 import St_6_to_Next_Spike_g2

# st = St_6_to_io()
st = St_6_to_Next_Spike_g2()


if __name__ == '__main__':
    set_log_level('DEBUG')

    init_funcs()

    logging.info('init functions')

    code = 'tq01'
    db_path = '../../../data/data_similarity/'
    db_name = 'data_similarity.db'

    hist = {}
    init_data('%s%s.csv' % (db_path, code), hist)
    db = DaoDbUtils(db_path)
    # spikes = db.load_spike_between(code, '5', '2019-01-01', '2021-01-01')
    spikes = db.load_spike_between(code, '5', '2019-01-01', '2021-01-01')

    logging.info('load history data')

    descs = []

    descs.append('root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|division|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|param 7')
    # descs.append('not|greater|param 1|param 7')

    seeds = []
    for desc in descs:
        node, text = desc_to_node(desc)
        seeds.append(node)

    # one_round(seeds, spikes, hist)

    # (s, in_cnt, vic_cnt) = st.score(spikes, seeds[0], seeds[1], hist)
    # (s, in_cnt, vic_cnt) = st.score(spikes, seeds[0], hist)

    samples = gen_samples(spikes)
    answers, avg = st.get_answers(spikes, hist)

    start = time.time()
    evals = st.evaluate(samples, seeds[0])
    end = time.time()
    # print start, end

    for i in range(0, len(evals)):
        if evals[i][1] is not None:
            logging.debug('%s\t%f\t%f' % (evals[i][0], evals[i][1], answers[evals[i][0]]))

    st.score(evals, answers, avg)


