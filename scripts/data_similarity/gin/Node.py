# coding:utf-8

class Node(object):

    name = None
    func = None
    children = None
    desc = None

    def __init__(self, function, name):
        self.func = function
        self.name = name
        self.desc = name
        self.children = []

    def add_child(self, node):
        self.children.append(node)
        self.desc += '|' + node.__repr__()

    def __repr__(self):
        return self.desc

    def evaluate(self, params):
        if self.name[:6] == 'param ':
            return self.func(params)
        elif self.name[:6] == 'const ':
            return self.func()
        else:
            rs = []
            for c in self.children:
                rs.append(c.evaluate(params))
            return self.func(rs)

    def length(self):
        return len(self.desc.split('|'))

def show_node(node, prefix=''):
    # print node
    print prefix, node
    for c in node.children:
        show_node(c, prefix + '\t')


def copy_node(node):
    new_node = Node(node.func, node.name)
    for child in node.children:
        new_child = copy_node(child)
        new_node.add_child(new_child)
    return new_node


def update_desc(node):
    desc = node.name
    for child in node.children:
        update_desc(child)
        desc += '|' + child.desc
    node.desc = desc
