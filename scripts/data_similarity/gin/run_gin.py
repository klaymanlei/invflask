# coding:utf-8

import datetime
import logging

from node_factory import *
from sample_factory import *
from scripts.data_similarity.st.spike_terms.dao.db_utils import *
from scripts.data_similarity.st.spike_terms.dao.Record import Record
from scripts.data_similarity.gin.st.st_6_to_next_spike_g2 import St_6_to_Next_Spike_g2
from evolution import Evolution

turn = 3000
group_size = 50
init_tree_depth = 5
gen_child_depth = 3
param_cnt = 12
func_rate = 0.8
param_rate = 0.7
mutation_rate = 0.1
crossing_rate = 0.2
crossing_step = 0.05
code = 'tq01'
# st = St_6_to_io()
st = St_6_to_Next_Spike_g2()


def setup(code):
    init_funcs()
    logging.info('init functions')

    db_path = '../../../data/data_similarity/'
    db_name = 'data_similarity.db'

    hist = {}
    init_data('%s%s.csv' % (db_path, code), hist)
    db = DaoDbUtils(db_path)
    spikes = db.load_spike_between(code, '5', '2017-01-01', '2020-01-01')
    # spikes = db.load_spike_between(code, '5', '2018-01-01', '2021-01-01')
    # spikes = db.load_spike_between(code, '5', '2016-01-01', '2021-01-01')

    logging.info('load history data')

    nodes = gen_nodes(group_size, init_tree_depth, param_cnt, func_rate, param_rate)
    # nodes = manual_nodes()

    logging.info('generate nodes')
    return (nodes, hist, spikes)


def manual_nodes():
    descs = []
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|division|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|abs|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|param 7|param 7|mod|param 0|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|division|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|mod|const 1.000000|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|or|param 7|const 0.000000|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|xor|abs|xor|const 1.000000|greater|param 7|subtract|subtract|param 7|greater|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|or|param 7|neg|param 7|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|xor|abs|xor|const 1.000000|greater|param 7|subtract|subtract|param 7|greater|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|abs|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|mod|add|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|equal|division|abs|param 8|param 0|param 6|multiply|param 1|const -8.824065|xor|greater|abs|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|param 7|param 7|mod|mod|const 1.000000|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|const 1.000000|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|abs|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|mod|greater|add|param 2|param 7|const -7.880701|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|mod|const 1.000000|add|param 2|param 7|xor|subtract|param 2|greater|greater|or|param 7|neg|param 7|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|xor|abs|xor|const 1.000000|greater|param 7|subtract|subtract|param 7|greater|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|add|add|add|subtract|mod|mod|const -7.880701|mod|const 1.000000|add|param 2|param 7|mod|const 1.000000|add|param 2|param 7|xor|greater|param 2|greater|greater|const 1.000000|equal|param 2|param 6|multiply|param 1|const -8.824065|xor|greater|abs|greater|const 1.000000|greater|param 7|subtract|subtract|param 7|xor|const 1.000000|greater|subtract|param 8|const 1.000000|const 1.000000|neg|division|param 2|param 6|equal|const 1.000000|param 6|equal|subtract|param 8|param 0|param 6|mod|const 1.000000|param 7|mod|add|const 1.000000|param 7|param 7|param 7')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|param 0')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|not|param 9')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|xnor|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|param 3')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|subtract|param 4|not|mod|subtract|param 2|param 0|param 0|add|param 9|param 7|not|param 9')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|subtract|param 4|not|mod|subtract|param 2|param 0|param 0|add|param 9|param 7|add|param 9|param 0')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|and|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|not|mod|param 2|param 9')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|and|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|not|mod|param 2|not|param 9')
    descs.append(
        'root|if|subtract|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|xnor|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|not|equal|param 9|division|const -0.491325|param 6')
    descs.append(
        'root|if|greater|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|and|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|add|param 9|param 7|not|mod|param 2|not|param 9')
    descs.append(
        'root|if|subtract|subtract|param 9|const -0.298576|param 3|neg|add|add|param 5|add|division|param 5|mod|mod|param 1|subtract|subtract|const 1.000000|multiply|subtract|param 4|param 1|const -8.296111|not|mod|param 2|param 0|const 6.884243|param 9|param 7|if|xnor|param 3|subtract|param 4|not|mod|param 2|param 0|add|param 9|param 7|not|equal|subtract|param 9|param 6|division|const -0.491325|param 6')

    nodes = []
    for d in descs:
        node, desc = desc_to_node(d)
        nodes.append(node)
    return nodes


def set_log_level(level):
    fmt_str = '%(asctime)s %(name)s(%(lineno)d) %(levelname)s %(message)s'
    if level == 'DEBUG':
        logging.basicConfig(level=logging.DEBUG, format=fmt_str)
    elif level == 'INFO':
        logging.basicConfig(level=logging.INFO, format=fmt_str)


def init_data(path, hist, end_date='2022-08-04'):
    last = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
    input = open(path, 'r')
    last_date = None
    for line in input:
        data = line.split(',')
        date = datetime.datetime.strptime(data[1], '/' in data[1] and '%Y/%m/%d' or '%Y-%m-%d').date()
        r = Record(data[0], date, float(data[5]), float(data[3]), float(data[4]), float(data[2]), long(data[10]))
        if date <= last and len(data) > 11 and float(data[2]) > 0:
            hist[date.strftime('%Y-%m-%d')] = (r, last_date)
            last_date = date.strftime('%Y-%m-%d')
    input.close()


if __name__ == '__main__':
    set_log_level('INFO')
    nodes, hist, spikes = setup(code)
    samples = gen_samples(spikes)
    answers, avg = st.get_answers(spikes, hist)
    sead_bag = None
    evo = Evolution(group_size=100)

    for i in range(0, turn):
        seed_bag = st.one_lap(nodes, samples, answers)

        if (i + 1) % 10 == 0:
            for category in seed_bag.categories():
                logging.info('round %d %s %s' % ((i + 1), category, seed_bag.read_note(category)))
                # if len(seed_bag.take_out(category)) >= 1:
                #     logging.info('node: %s', seed_bag.take_out(category)[0].desc)

        if (i + 1) % 100 == 0:
            logging.info('round %d winners' % (i + 1))
            for category in seed_bag.categories():
                logging.info(category)
                logging.info(seed_bag.take_out(category)[0].desc)
                for node in seed_bag.take_out(category):
                    logging.info('(len: %d) %s' % (node.length(), node.desc[:50]))

        next_gen = evo.evolve(seed_bag, functions)
        if next_gen is not None:
            nodes = next_gen
            if (i + 1) % 100 == 0:
                logging.info('group size: %d' % len(nodes))
            continue

    logging.info('final result')
    for category in seed_bag.categories():
        logging.info(category)
        for node in seed_bag.take_out(category):
            logging.info(node.desc)
