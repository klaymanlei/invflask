# coding:utf-8

import logging

from node_factory import *


class Evolution(object):
    mutation_rate = 0.1
    crossing_rate = 0.2
    crossing_step = 0.05
    gen_child_depth = 3
    param_cnt = 12
    func_rate = 0.8
    param_rate = 0.7
    group_size = 40
    init_tree_depth = 5

    def __init__(self,
                 mutation_rate=0.1,
                 crossing_rate=0.2,
                 crossing_step=0.05,
                 gen_child_depth=3,
                 param_cnt=12,
                 func_rate=0.8,
                 param_rate=0.7,
                 group_size=40,
                 init_tree_depth=5
                 ):
        self.mutation_rate = mutation_rate
        self.crossing_rate = crossing_rate
        self.crossing_step = crossing_step
        self.gen_child_depth = gen_child_depth
        self.param_cnt = param_cnt
        self.func_rate = func_rate
        self.param_rate = param_rate
        self.group_size = group_size
        self.init_tree_depth = init_tree_depth

    def evolve(self, seed_bag, functions):
        seeds = {}
        logging.debug('seed bag')
        for seed in seed_bag.pour_out():
            logging.debug('last gen:' + seed.desc)
            seeds[seed.desc] = seed
        # crossing
        logging.debug('crossing')
        champions = {}
        for category in seed_bag.categories():
            champions[category] = seed_bag.take_out(category)[0]
        for category in champions:
            champion = champions[category]
            for winner in seed_bag.pour_out():
                if winner.desc == champion.desc:
                    partner = seed_bag.random_seed(category, winner)
                else:
                    partner = champion
                next_gen = crossing(winner, partner, self.crossing_rate, self.crossing_step)
                logging.debug('node1: %s' % winner.desc)
                logging.debug('node2: %s' % partner.desc)
                logging.debug('crossing: %s' % next_gen.desc)
                seeds[next_gen.desc] = next_gen
        # cut
        cut(champions, seeds, self.mutation_rate)
        # for category in champions:
        #     champion = champions[category]
        #     for i in range(0, 5):
        #         clone = copy_node(champion)
        #         parent, idx = pick_child([clone], self.mutation_rate, 0)
        #         rand = random.random() * 20 - 10
        #         parent.children[idx] = Node(const_node(rand), 'const %f' % rand)
        #         update_desc(clone)
        #         seeds[clone.desc] = clone

        # mutation
        logging.debug('mutation')
        for winner in seed_bag.pour_out():
            if len(winner.children) == 0:
                continue
            clone = copy_node(winner)
            parent, idx = pick_child([clone], self.mutation_rate, 0)
            mutation = parent.children[idx]
            f = random.randrange(len(functions))
            func = functions[f]
            node = Node(func[0], func[2])
            for i in range(0, func[1]):
                if i < len(mutation.children):
                    node.add_child(mutation.children[i])
                else:
                    node.add_child(random_tree(self.gen_child_depth, self.param_cnt, self.func_rate, self.param_rate))
            parent.children[idx] = node
            update_desc(clone)
            logging.debug('source: %s' % winner.desc)
            logging.debug('mutation: %s' % clone.desc)
            seeds[clone.desc] = clone

        # rand_cnt = 0
        # logging.debug('random')
        # while len(seeds) < self.group_size:
        #     node = gen_node(self.init_tree_depth, self.param_cnt, self.func_rate, self.param_rate)
        #     logging.debug('random: %d/%d' % (len(seeds), self.group_size))
        #     seeds[node.desc] = node
        #     rand_cnt += 1
        # logging.debug('replenish %d rand nodes' % rand_cnt)

        nodes = []

        for n in seeds.values():
            nodes.append(simplify(n))

        del seeds
        return nodes


def cut(champions, seeds, mutation_rate):
    for category in champions:
        champion = champions[category]
        # 每个champion做5次cut
        for i in range(0, 5):
            clone = copy_node(champion)
            mutated = False
            children = []
            children.extend(clone.children)
            while len(children) > 0:
                g_children = children[0].children
                for i in range(0, len(g_children)):
                    if g_children[i].name in ['if', 'greater', 'equal', 'and', 'or', 'not', 'xnor', 'xor']:
                        rand = random.random()
                        if rand < mutation_rate:
                            rand_const = random.randint(0, 1)
                            if g_children[i].name == 'if':
                                children[0].children[i] = g_children[i].children[rand_const + 1]
                            else:
                                children[0].children[i] = Node(const_node(rand_const), 'const %f' % rand_const)
                            mutated = True
                            break
                if mutated:
                    update_desc(clone)
                    seeds[clone.desc] = clone
                    break
                children.pop(0)
                children.extend(g_children)


if __name__ == '__main__':
    init_funcs()
    desc = 'root|if|add|multiply|const 4.077460|division|const 1.620911|division|const 1.815631|division|add|param 3|add|param 3|mod|division|add|param 9|const 5.841937|param 6|param 4|add|xnor|greater|const -7.692875|division|const 8.431604|division|const 8.810108|abs|not|subtract|const 1.000000|add|param 3|mod|division|const 6.833139|division|const 1.000000|division|const 1.000000|param 6|param 4|const -5.485538|division|abs|param 9|param 3|const 4.077460|if|add|multiply|const 6.355486|if|add|multiply|const 6.355486|division|const 3.429314|division|const 9.570333|division|add|param 3|add|param 3|mod|division|const 6.089680|param 6|const 2.314851|add|xnor|greater|const -1.366451|add|const -2.197200|division|const 0.838725|xnor|const 6.016650|abs|xnor|const 3.502558|add|xnor|subtract|const 1.000000|add|param 3|mod|division|const -8.245347|division|or|const -7.580423|or|const -2.073689|abs|greater|const 7.834448|param 4|const -3.188912|param 4|param 3|const 0.000000|const -5.406515|division|add|param 9|xnor|const 5.463913|param 3|param 3|const 2.900660|division|const 3.066779|division|const 7.916466|mod|const 7.076097|division|const 6.607571|add|xnor|greater|param 3|add|const -0.356420|division|const 7.644808|greater|mod|const 7.734386|xnor|const 3.738213|add|xnor|subtract|const 1.000000|add|param 3|mod|const 29.717612|param 4|const -3.867944|division|add|param 9|const 0.000000|param 3|const -2.678768|param 3|division|add|param 9|xnor|add|division|xor|const -8.101503|xor|const -9.585801|and|const 9.096126|abs|add|const 0.000000|not|abs|xor|const 1.730478|add|param 3|const 0.155931|subtract|mod|division|const 5.910857|division|const 1.000000|param 3|const 1.000000|division|const 1.000000|param 6|const -1.532340|param 3|param 3|const -6.361670|const 3.492601|if|add|const 4.968632|neg|division|const 3.635469|add|param 3|mod|const -1.702352|if|add|param 3|add|const 0.000000|mod|xor|const 2.188881|division|add|param 3|subtract|const 3.958736|division|param 9|add|param 3|mod|param 3|mod|neg|add|param 9|const 5.841937|const 1.232567|const 8.197496|const 5.559450|division|const -9.600530|division|const 1.210399|if|xor|const 0.001891|abs|equal|param 0|param 8|if|division|const 5.695593|division|const 1.684568|division|const 7.916466|multiply|const 7.919499|mod|const 4.597447|division|const 5.620921|add|const 0.000000|division|const 1.643170|param 3|mod|const -8.399440|division|const -2.813805|mod|const 4.946850|division|const 7.916466|division|const 1.899653|multiply|const 3.263813|abs|mod|subtract|const 9.170885|division|const 4.214749|xnor|xnor|const 4.185891|and|add|or|greater|const 1.000000|mod|division|const 5.497394|division|const 1.000000|division|const 1.000000|param 6|const 9.085277|const -7.241806|xnor|and|const 2.957176|abs|add|neg|greater|const 1.000000|division|const 4.193332|param 6|const 1.000000|const 2.153378|const 4.440072|greater|mod|division|const 5.965441|division|add|division|const 7.650806|division|const 1.963166|param 6|const 1.000000|param 3|const 4.783721|division|const 1.000000|add|param 8|const 1.521750|const 5.558481|const 0.000000|const 2.466174|const -7.898155|if|add|multiply|const 4.589505|add|multiply|const 9.923123|division|const 4.047936|division|const 7.359554|mod|const 9.911663|division|const 0.941196|add|const 1.000000|division|add|param 9|xnor|add|division|const 1.000000|subtract|const 0.742051|division|const 7.965925|param 6|const -0.884680|param 3|param 3|const 2.126972|const -6.380292|division|add|param 3|xor|const -8.537115|mod|xor|const 5.205281|division|const 9.281851|division|add|add|const -0.499619|subtract|const 8.349875|division|param 9|add|param 3|mod|const 5.089405|add|const -5.974776|param 4|const -3.074894|const -6.059973|const 6.929829|if|neg|division|const -6.705136|and|const 2.255650|division|const 6.049894|division|add|const -1.010233|mod|multiply|greater|const 3.958736|division|param 9|const 0.124927|const -9.900195|const 9.847092|if|xor|const 0.979487|mod|equal|param 0|param 8|param 3|if|division|const 9.387783|division|const -0.401562|division|const 7.916466|multiply|const 0.551499|mod|const -9.567642|division|const 0.167210|add|const 1.000000|division|add|param 9|xnor|add|const 0.000000|division|add|param 9|abs|greater|const 1.000000|division|const 1.744209|param 6|param 3|param 3|param 3|multiply|const 9.668672|mod|const -0.379617|mod|const 2.857161|division|const 3.523805|division|const 8.067397|multiply|const 3.263813|multiply|const 2.440771|mod|const 1.229504|add|const 0.167210|add|const 1.000000|division|add|param 9|xnor|add|division|xor|const -9.822758|xor|const 0.000000|and|const 7.253850|abs|subtract|const 0.000000|not|abs|xor|const 7.515799|add|param 3|const 0.155931|subtract|mod|add|const 5.910857|division|const 1.000000|param 3|const 1.000000|division|const 1.000000|param 6|const -1.039832|param 3|param 3|const 6.518779|const 4.751740|if|division|const 4.186826|division|const 8.008469|multiply|const 4.648473|multiply|const 6.060837|division|const 8.892692|division|const 7.004964|mod|const 4.946850|division|const 3.305538|greater|const 6.125718|multiply|abs|multiply|const 5.744006|division|const -4.295037|xnor|xnor|const 4.185891|and|add|or|greater|const 1.000000|mod|division|const 5.224465|division|const 1.000000|division|const 1.000000|param 6|const 9.085277|const -7.241806|xnor|and|const 2.483825|abs|add|neg|greater|const 1.000000|division|add|abs|param 4|const 1.000000|param 6|const 1.000000|const 1.734636|const 4.440072|greater|mod|division|const 5.965441|division|const 1.000000|param 3|const 4.783721|division|const 1.000000|add|param 8|const 1.521750|const 0.009737|const 0.756026|division|add|multiply|const 7.818746|multiply|add|param 3|const 1.285284|division|const 1.870322|division|const 7.916466|mod|const 7.076097|division|const 6.607571|add|const 0.000000|division|add|param 9|const 0.000000|param 3|const 2.900660|const 6.688209|const -4.636286|const 0.969684|if|add|add|const 4.454814|division|const 9.570729|division|add|add|const 0.545389|subtract|const 3.958736|division|const -1.228983|add|const 0.462958|mod|const -3.930155|add|const -5.974776|param 4|const -0.797417|const -0.295931|division|add|const -1.271106|subtract|const 3.958736|division|param 9|add|param 3|const 0.004296|const -1.738605|if|add|const 4.077460|mod|const -5.574096|division|param 9|add|param 3|const -0.073737|if|abs|multiply|const 9.923123|add|multiply|const 9.923123|division|const 2.825253|division|const 7.359554|and|const 9.911663|division|const 0.941196|add|const 0.000000|division|add|param 9|xnor|add|const -9.806904|division|add|param 9|const 1.000000|param 3|param 3|param 3|const 2.126972|if|not|division|const -6.705136|and|const 4.736084|division|const 6.049894|and|const 8.783756|if|division|subtract|const -7.106676|subtract|const 0.000000|param 6|const 46.340911|multiply|const 9.668672|mod|const -6.276974|mod|const 2.857161|division|const 6.723016|division|const 3.523805|division|const 8.067397|multiply|const 6.368340|multiply|const 2.440771|mod|const 1.229504|division|const 0.167210|add|const 0.000000|division|add|param 9|xnor|add|division|const 1.000000|subtract|mod|add|const 5.910857|division|const 1.000000|param 3|const 1.000000|division|const 1.000000|param 6|division|add|param 9|const 1.000000|const -0.646061|param 3|param 3|const 25.476425|const 3.545674|const -4.636286|const 7.249033|const -9.447431|if|if|add|const 1.546172|division|add|const -9.020195|subtract|const 1.000000|division|param 9|add|param 3|mod|param 3|const 9.977039|const -8.708918|division|const 5.208048|division|const 5.447312|if|division|const 9.387783|division|subtract|const -3.989652|subtract|const 2.600199|param 6|const 7.391322|const 3.378168|multiply|const 9.283608|mod|const -9.649090|division|const 7.004964|and|const 7.916466|add|const 1.899653|multiply|const 3.298456|abs|mod|subtract|const 8.996828|division|const 6.639198|equal|const 1.000000|greater|mod|division|const 5.965441|division|const 1.000000|param 3|const 4.783721|division|const 1.000000|abs|param 8|const 6.199700|const 5.814596|add|param 3|add|const -0.181381|greater|add|const 2.280083|division|add|const -0.430164|xnor|add|division|const 1.000000|subtract|const 0.752760|division|const 1.000000|param 6|const 3.358802|param 3|param 3|const 9.095712|const -5.680562|if|multiply|const 0.768880|multiply|add|param 3|const 0.239238|division|const 3.944439|division|const 7.916466|mod|const -8.005625|division|const 6.607571|add|xnor|greater|param 3|const 0.348903|param 3|division|add|param 9|xnor|add|division|xor|const -7.704576|xor|const -9.585801|and|const 8.889646|abs|add|const 0.000000|not|abs|xor|const 7.515799|and|param 3|const 0.155931|subtract|mod|division|const 5.910857|division|const 1.000000|param 3|const 1.000000|division|const 1.000000|param 6|division|add|param 9|xnor|greater|const 1.000000|division|const 1.744209|param 6|param 3|param 3|param 3|param 3|if|add|const 4.077460|if|add|multiply|const 4.077460|division|const 1.656324|division|const 1.815631|division|const 11.035874|add|const 1.000000|division|add|param 9|xnor|add|division|const 1.000000|subtract|neg|division|const 5.910857|division|const 1.000000|param 3|const -0.121216|const 1.695877|param 3|param 3|const -0.688480|abs|division|const 6.505246|division|const 2.987854|multiply|const 0.974936|mod|const 4.922711|division|const -2.348096|mod|const 4.946850|division|const 0.227413|division|const 2.789597|multiply|const -9.421102|neg|abs|subtract|const -9.233422|division|const -1.847359|xnor|const 0.191536|greater|mod|division|const 5.965441|division|const 0.743457|param 3|const 4.783721|const 0.191361|const 6.341491|const 2.550977|const -6.750924|const -6.750924|neg|add|const -0.181381|greater|division|add|const -0.430164|xnor|add|division|const 1.000000|subtract|const 0.752760|division|const 1.000000|param 6|const 2.595560|const -1.097909|const -0.674222|division|param 9|add|param 3|add|param 3|mod|neg|add|param 9|const 5.841937|param 4'
    node, text = desc_to_node(desc)
    champions = {'test': node}
    seeds = {}
    cut(champions, seeds, 0.1)
    for seed in seeds:
        print seeds[seed].desc
