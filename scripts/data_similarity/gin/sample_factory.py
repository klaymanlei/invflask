# coding:utf-8

def gen_samples(spikes):
    samples = {}
    params = []
    # scores = []
    last_spike = None
    # last_rs = None
    days = 0
    for spike in spikes:
        if spike.direction != '0':
            if last_spike is not None:
                params.append(days)
                params.append((spike.value - last_spike.value) / last_spike.value)
                if len(params) == 14:
                    samples[spike.date] = params[:12]
                    new_sample = []
                    new_sample.extend(params[2:])
                    params = new_sample
            last_spike = spike
            days = 0
        days += 1

    return samples

