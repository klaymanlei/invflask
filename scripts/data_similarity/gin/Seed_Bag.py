# coding:utf-8

import random

class Seed_Bag(object):
    # seeds = None
    # note_book = None

    def __init__(self):
        self.seeds = {}
        self.note_book = {}

    def put_in(self, seed, category):
        if category not in self.seeds:
            self.seeds[category] = []
        self.seeds[category].append(seed)

    def note(self, category, note):
        self.note_book[category] = note

    def take_out(self, category):
        return self.seeds[category]

    def categories(self):
        return self.seeds.keys()

    def read_note(self, category):
        return self.note_book[category]

    def random_seed(self, category, except_one):
        pick = None
        while pick is None or pick.desc == except_one.desc:
            idx = random.randrange(len(self.seeds[category]))
            pick = self.seeds[category][idx]
        return pick

    def pour_out(self):
        rs = []
        for category in self.seeds:
            for seed in self.seeds[category]:
                if seed not in rs:
                    rs.append(seed)
        return rs
