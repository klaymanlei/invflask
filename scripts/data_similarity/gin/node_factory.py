# coding:utf-8

import random
import logging

from Node import *

functions = []
func_dict = {}

def init_funcs():
    func_add = lambda params: params[0] + params[1] if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_add, 2, 'add'))
    func_dict['add'] = (func_add, 2)

    func_sub = lambda params: params[0] - params[1] if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_sub, 2, 'subtract'))
    func_dict['subtract'] = (func_sub, 2)

    func_mul = lambda params: params[0] * params[1] if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_mul, 2, 'multiply'))
    func_dict['multiply'] = (func_mul, 2)

    func_div = lambda params: (float(params[0]) / params[1] if params[1] != 0 else None) if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_div, 2, 'division'))
    func_dict['division'] = (func_div, 2)

    func_if = lambda params: (params[1] if params[0] is not None and params[0] > 0 else params[2]) if (params is not None and len(params) == 3) else None
    functions.append((func_if, 3, 'if'))
    func_dict['if'] = (func_if, 3)

    func_gt = lambda params: (1 if params[0] > params[1] else 0) if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_gt, 2, 'greater'))
    func_dict['greater'] = (func_gt, 2)

    func_eq = lambda params: (1 if params[0] is not None and params[1] is not None and params[0] == params[1] else 0) if (params is not None and len(params) == 2) else None
    functions.append((func_eq, 2, 'equal'))
    func_dict['equal'] = (func_eq, 2)

    func_abs = lambda params: abs(params[0]) if (params is not None and len(params) == 1 and params[0] is not None) else None
    functions.append((func_abs, 1, 'abs'))
    func_dict['abs'] = (func_abs, 1)

    func_mod = lambda params: (params[0] % params[1] if params[1] != 0 else None) if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_mod, 2, 'mod'))
    func_dict['mod'] = (func_mod, 2)

    func_and = lambda params: (1 if (params[0] is not None and params[1] is not None and params[0] > 0 and params[1] > 0) else 0) if (params is not None and len(params) == 2) else None
    functions.append((func_and, 2, 'and'))
    func_dict['and'] = (func_and, 2)

    func_or = lambda params: (1 if (params[0] is not None and params[1] is not None and (params[0] > 0 or params[1] > 0)) else 0) if (params is not None and len(params) == 2) else None
    functions.append((func_or, 2, 'or'))
    func_dict['or'] = (func_or, 2)

    func_not = lambda params: (1 if (params[0] <= 0) else 0) if (params is not None and len(params) == 1 and params[0] is not None) else None
    functions.append((func_not, 1, 'not'))
    func_dict['not'] = (func_not, 1)

    func_xnor = lambda params: (1 if (params[0] is not None and params[1] is not None and ((params[0] > 0 and params[1] > 0) or (params[0] <= 0 and params[1] <= 0))) else 0) if (params is not None and len(params) == 2) else None
    functions.append((func_xnor, 2, 'xnor'))
    func_dict['xnor'] = (func_xnor, 2)

    func_xor = lambda params: (1 if (params[0] is not None and params[1] is not None and ((params[0] > 0 and params[1] <= 0) or (params[0] <= 0 and params[1] > 0))) else 0) if (params is not None and len(params) == 2 and params[0] is not None and params[1] is not None) else None
    functions.append((func_xor, 2, 'xor'))
    func_dict['xor'] = (func_xor, 2)

    func_neg = lambda params: -params[0] if (params is not None and len(params) == 1 and params[0] is not None) else None
    functions.append((func_neg, 1, 'neg'))
    func_dict['neg'] = (func_neg, 1)


def param_node(index):
    return lambda params: len(params) >= index + 1 and params[index] or None


def const_node(value):
    return lambda: value


def root_node():
    return lambda params: params[0]


def gen_nodes(group_size, init_tree_depth, param_cnt, func_rate, param_rate):
    nodes = []

    while len(nodes) < group_size:
        node = gen_node(init_tree_depth, param_cnt, func_rate, param_rate)
        logging.debug(node.desc)
        nodes.append(node)
    return nodes


def gen_node(init_tree_depth, param_cnt, func_rate, param_rate):
    root = Node(root_node(), 'root')
    node = random_tree(init_tree_depth, param_cnt, func_rate, param_rate)
    root.add_child(node)
    return root

def random_tree(depth, param_cnt, func_rate, param_rate):
    if random.random() < func_rate and depth > 0:
        f = random.randrange(len(functions))
        func = functions[f]
        node = Node(func[0], func[2])
        for i in range(0, func[1]):
            node.add_child(random_tree(depth - 1, param_cnt, func_rate, param_rate))
        return node
    elif random.random() < param_rate:
        rand = random.randint(0, param_cnt - 1)
        return Node(param_node(rand), 'param %d' % rand)
    else:
        rand = random.random() * 20 - 10
        return Node(const_node(rand), 'const %f' % rand)


def pick_child(nodes, rate, step=0.1):
    children = []
    for node in nodes:
        for c in node.children:
            if len(c.children) > 0:
                children.append(c)
    r = random.random()
    if r < rate or len(children) == 0:
        i = random.randrange(0, len(nodes))
        picked = nodes[i]
        idx = random.randrange(0, len(picked.children))
        return (picked, idx)
    else:
        return pick_child(children, rate + step, step)


def crossing(node1, node2, rate, step=0.1):
    node = copy_node(node1)
    if len(node1.children) == 0:
        return node2
    elif len(node2.children) == 0:
        exchange1, idx1 = pick_child([node], rate, step)
        exchange1.children[idx1] = node2
        update_desc(node)
        return node
    else:
        exchange1, idx1 = pick_child([node], rate, step)
        exchange2, idx2 = pick_child([node2], rate, step)
        exchange1.children[idx1] = exchange2.children[idx2]
        update_desc(node)
        return node


def pick_node(nodes, partner):
    if len(nodes) == 0:
        return None
    node = partner
    while node.desc == partner.desc:
        node = nodes[random.randrange(len(nodes))]
    return node


def leaf(desc_str):
    if desc_str[:5] == 'param':
        param = int(desc_str[6:])
        node = Node(param_node(param), desc_str)
        return node
    elif desc_str[:5] == 'const':
        param = float(desc_str[6:])
        node = Node(const_node(param), desc_str)
        return node


def desc_to_node(desc_str):
    if desc_str == '':
        return None, None
    if '|' not in desc_str:
        return leaf(desc_str), ''
    idx = desc_str.index('|')
    part1 = desc_str[:idx]
    part2 = desc_str[idx + 1:]
    if part1[:5] == 'param' or part1[:5] == 'const':
        return leaf(part1), part2
    elif part1 == 'root':
        node = Node(root_node(), 'root')
        child, part2 = desc_to_node(part2)
        node.add_child(child)
        update_desc(node)
        return node, part2
    else:
        node = Node(func_dict[part1][0], part1)
        for i in range(0, func_dict[part1][1]):
            child, part2 = desc_to_node(part2)
            node.add_child(child)
        update_desc(node)
        return node, part2


def simplify(node):
    if node.desc[:5] == 'param' or node.desc[:5] == 'const':
        return node
    children = node.children
    new_children = []
    all_const = True
    children_value = []
    for child in children:
        if child.desc[:5] != 'param' and child.desc[:5] != 'const':
            child = simplify(child)

        if child.desc[:5] == 'const':
            children_value.append(child.func())
        else:
            all_const = False

        new_children.append(child)

    # 所有子节点都是常量，则该节点等价于一个常量节点
    if all_const and node.name != 'root':
        rs = node.func(children_value)
        if rs is None:
            update_desc(node)
            return node
        return Node(const_node(rs), 'const %f' % rs)

    node.children = new_children
    if node.name == 'subtract':
        node = simplify_subtract(node)
    elif node.name == 'multiply':
        node = simplify_multiply(node)
    elif node.name == 'division':
        node = simplify_division(node)
    elif node.name == 'if':
        node = simplify_if(node)
    elif node.name == 'equal':
        node = simplify_equal(node)
    elif node.name == 'abs':
        node = simplify_abs(node)
    elif node.name == 'and':
        node = simplify_and(node)
    elif node.name == 'or':
        node = simplify_or(node)
    elif node.name == 'xnor':
        node = simplify_xnor(node)
    elif node.name == 'xor':
        node = simplify_xor(node)
    update_desc(node)
    return node


def simplify_subtract(node):
    children = node.children
    if children[0].desc == children[1].desc and children[0].desc[:5] == 'param':
        return Node(const_node(0), 'const %f' % 0)
    elif children[1].desc[:5] == 'const' and children[1].func() == 0:
        return children[0]
    return node


def simplify_equal(node):
    children = node.children
    if children[0].desc == children[1].desc and children[0].desc[:5] == 'param':
        return Node(const_node(1), 'const %f' % 1)
    return node



def simplify_abs(node):
    children = node.children
    if children[0].name == 'abs' or children[0].name == 'neg':
        node.children = children[0].children
        return Node(const_node(1), 'const %f' % 1)
    return node


def simplify_multiply(node):
    children = node.children
    if (children[0].desc[:5] == 'const' and children[0].func() == 0) \
            or (children[1].desc[:5] == 'const' and children[1].func() == 0):
        return Node(const_node(0), 'const %f' % 0)
    elif children[0].desc[:5] == 'const' and children[0].func() == 1:
        return children[1]
    elif children[1].desc[:5] == 'const' and children[1].func() == 1:
        return children[0]
    return node


def simplify_division(node):
    children = node.children
    if children[0].desc[:5] == 'const' and children[0].func() == 0:
        return Node(const_node(0), 'const %f' % 0)
    elif children[1].desc[:5] == 'const' and children[1].func() == 1:
        return children[0]
    elif children[0].desc == children[1].desc and children[0].desc[:5] == 'param':
        return Node(const_node(1), 'const %f' % 1)
    return node


def simplify_if(node):
    children = node.children
    if children[0].desc[:5] == 'const' and children[0].func() > 0:
        return children[1]
    elif children[0].desc[:5] == 'const' and children[0].func() <= 0:
        return children[2]
    return node


def simplify_and(node):
    children = node.children
    if children[0].desc[:5] == 'const' and children[0].func() <= 0:
        return Node(const_node(0), 'const %f' % 0)
    elif children[1].desc[:5] == 'const' and children[1].func() <= 0:
        return Node(const_node(0), 'const %f' % 0)
    return node


def simplify_or(node):
    children = node.children
    if children[0].desc[:5] == 'const' and children[0].func() > 0:
        return Node(const_node(1), 'const %f' % 1)
    elif children[1].desc[:5] == 'const' and children[1].func() > 0:
        return Node(const_node(1), 'const %f' % 1)
    return node


def simplify_xnor(node):
    children = node.children
    if children[0].desc == children[1].desc and children[0].desc[:5] == 'param':
        return Node(const_node(1), 'const %f' % 1)
    return node


def simplify_xor(node):
    children = node.children
    if children[0].desc == children[1].desc and children[0].desc[:5] == 'param':
        return Node(const_node(0), 'const %f' % 0)
    return node


if __name__ == '__main__':
    init_funcs()

    desc = 'equal|abs|neg|abs|neg|const 0.244413|not|param 2'
    node, desc = desc_to_node(desc)
    print simplify(node).desc


    # 验证simplify
    # desc = 'root|if|division|division|division|param 7|add|add|add|add|if|and|division|const -9.937725|division|const 1.692600|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|const 6.276251|const 5.493134|param 3|const 9.358265|add|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 3.679218|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|param 3|param 5|param 5|param 3|param 5|const 4.162460|const 6.404151|param 7|param 5|param 3|division|const 7.034090|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|multiply|or|equal|multiply|or|const -1.925094|const -3.469115|const -9.276848|greater|param 4|subtract|division|if|const 5.667490|param 8|const 1.930429|xnor|xnor|const -2.245287|const 6.858113|param 7|const 0.191376|const -1.322631|param 3|if|and|division|add|add|add|if|and|division|const -9.937725|division|const 1.692600|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|const 6.276251|const 5.493134|param 3|const 9.358265|add|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 3.679218|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|param 3|param 5|param 5|param 3|param 5|const 4.162460|const 6.404151|param 7|division|const 7.034090|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|multiply|or|equal|multiply|or|const -1.925094|const -3.469115|const -9.276848|const 8.026675|const -1.322631|param 3|and|division|add|add|subtract|if|and|division|const -8.643777|division|const 5.800230|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|const 6.276251|const 5.688339|param 3|const 9.358265|add|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 2.416200|add|subtract|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|param 3|const -6.449225|const -0.965592|param 3|param 5|const 9.414961|const 0.998070|const 8.472057|division|const 7.034090|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|division|or|equal|const -4.535974|const 8.026675|const -1.322631|const -7.105835|const 8.864971|if|const 2.644707|add|and|const 9.305950|neg|const 2.033576|add|add|add|division|const -3.044456|param 0|param 7|param 7|add|if|and|division|if|division|param 7|add|add|add|add|add|division|param 4|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|param 7|const 4.162460|neg|const -3.231682|const 9.459765|add|or|subtract|param 10|not|param 5|or|const -9.903997|abs|param 8|param 5|multiply|const 4.526069|param 3|add|multiply|subtract|param 2|param 4|param 0|and|xnor|const -2.765599|const -5.641246|const 4.615017|division|const 1.878919|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|const -3.736697|subtract|const -3.743789|const 3.266637|param 3|const 9.358265|if|abs|neg|division|division|const -5.878664|multiply|or|greater|param 4|division|subtract|equal|param 3|const -8.894347|param 2|const -9.068711|not|abs|add|mod|param 0|const -0.743565|mod|param 3|param 10|const -8.991177|add|const -9.951843|multiply|const 3.785506|param 3|subtract|add|add|add|add|const -0.007055|const -0.834027|division|const 1.554528|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|equal|multiply|or|greater|const 7.865362|division|subtract|subtract|const 4.988014|subtract|param 6|param 4|param 2|division|add|add|const 3.190179|param 7|division|division|param 7|subtract|xor|const -8.183680|division|division|neg|greater|const -4.626060|add|param 0|param 8|xnor|const 2.682251|mod|if|param 9|const -0.933081|const 4.855277|const 8.180728|not|multiply|subtract|add|division|neg|const 0.352722|subtract|xor|const 9.906360|const 0.352701|param 10|multiply|param 11|const 1.634790|abs|and|const 3.651512|greater|param 2|const -2.579968|const 5.235267|param 10|const 9.720887|not|const -8.850357|param 7|param 3|greater|param 4|subtract|division|if|const -6.448694|param 8|param 9|xnor|xnor|add|xnor|division|param 5|param 9|param 11|const -8.311456|const 6.858113|param 7|not|and|param 11|param 1|const -1.680516|const -4.176864|division|const 9.029817|add|add|const 6.179917|param 7|multiply|const 6.579929|param 3|const -5.087925|division|const 8.393194|add|add|const 8.853122|param 7|if|division|add|add|add|mod|and|division|const -8.643777|const -1.486136|const 9.358265|mod|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 3.679218|add|add|add|division|neg|equal|const 3.398621|param 3|param 0|param 7|param 7|param 3|const -6.449225|const -0.965592|param 3|const 9.414961|const 0.998070|const 8.881208|division|const 4.447625|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|multiply|or|const -2.625305|const -1.322631|param 3|if|and|division|add|add|add|if|and|division|const -8.643777|division|const 5.800230|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|const 6.276251|const 5.688339|param 3|const 9.358265|add|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 2.416200|add|subtract|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|param 3|const -6.449225|const -0.965592|param 3|param 5|const 9.414961|const 0.998070|const 8.472057|division|const 7.034090|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|multiply|or|equal|multiply|or|const -1.925094|const -3.469115|const -9.276848|const 8.026675|const -1.322631|param 3|const 8.864971|add|param 5|add|add|add|division|const -2.932493|param 0|if|and|division|add|add|add|if|and|division|const -8.643777|division|const 5.800230|add|add|const -1.646871|const -9.574306|const -0.455127|const 9.358265|add|const 6.363459|add|if|const 7.399673|if|const 5.579870|add|const 2.636673|division|const 2.416200|add|add|add|division|neg|const 1.560156|param 0|param 7|param 7|param 3|const -6.449225|const -0.965592|param 3|param 5|const 9.414961|const 0.998070|const 8.881208|division|const 7.034090|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|abs|multiply|or|equal|multiply|or|const -1.925094|const -3.469115|const -9.276848|greater|param 4|subtract|const -2.373974|const 0.191376|const -1.322631|const 8.893526|const 4.494099|if|const 3.992977|add|add|multiply|const 4.349047|if|abs|neg|const 6.335182|add|const -5.674883|division|const 8.393194|add|add|add|xnor|const -9.947457|param 0|param 7|param 7|param 3|param 5|param 3|neg|if|abs|abs|division|division|const -4.035765|abs|or|greater|param 4|division|subtract|const -4.428579|param 2|const -7.023392|const -6.000980|const 6.169361|subtract|const -7.582298|division|const -7.669462|division|const -5.593702|if|const 2.677232|add|const 4.572661|division|const -1.766189|multiply|greater|param 0|division|division|subtract|multiply|not|add|param 7|equal|const 6.399992|const -7.986738|subtract|param 6|param 4|param 8|abs|greater|param 4|division|subtract|multiply|mod|const -1.457067|const 6.736733|subtract|param 6|param 4|param 2|division|add|add|add|division|neg|division|param 3|param 3|add|const 3.969462|not|const -7.925462|param 7|param 7|neg|multiply|param 7|subtract|xor|const 0.419482|division|const 6.913763|not|multiply|subtract|add|division|neg|equal|const 0.566286|const 4.162452|const 8.099845|multiply|param 11|const 1.634790|subtract|xor|const -7.786228|const -5.729811|param 9|const 7.157739|const -9.248895|const 0.740700|abs|greater|param 4|add|add|add|add|const 4.446347|division|const 4.719803|add|add|add|const -0.031148|param 7|param 7|neg|subtract|xor|const -2.863686|const -4.716005|param 3|param 7|const -1.013481|const -8.083766|param 7|const -1.964476|param 5|const -4.967511|add|const -8.791578|division|const 9.699248|const -5.029646|param 7|add|if|and|const -8.416898|const 9.358265|and|const -1.671340|add|const -2.575994|division|const 8.650919|const -5.900535|param 5|add|and|const -5.452204|const -3.983049|param 3|const 0.046529|add|division|const 0.637448|add|not|division|neg|const 0.918398|param 0|param 7|division|const 9.699248|add|param 7|param 3|param 5|param 5|and|mod|and|division|division|division|param 7|add|add|const -3.124684|param 7|param 5|multiply|const 4.526069|param 3|division|const 1.878919|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|param 7|neg|multiply|or|equal|multiply|const -2.901037|param 3|const -4.154337|subtract|const -3.743789|const 3.266637|param 3|const 9.056446|if|const 3.513351|add|add|add|add|add|add|division|neg|equal|param 3|param 3|param 0|param 7|mod|add|division|const -1.752297|param 0|param 7|param 7|division|const 1.554528|add|add|const 7.975815|param 7|neg|multiply|or|equal|multiply|or|greater|const 7.865362|division|subtract|subtract|const 4.988014|subtract|param 6|param 4|param 2|division|add|add|add|const -6.792706|param 7|param 7|division|division|param 7|subtract|xor|const -8.183680|division|if|neg|greater|or|const -2.239755|param 5|add|param 0|param 8|xnor|or|division|const 2.394333|const -1.188858|not|param 11|const -7.174273|if|greater|abs|not|const -0.844597|abs|const -3.172390|param 6|const 6.659013|not|multiply|subtract|add|division|neg|equal|param 3|param 3|subtract|xor|const 9.906360|const 0.352701|param 10|multiply|param 11|const 1.634790|abs|and|const 3.651512|greater|param 2|const -2.579968|const 5.235267|param 10|multiply|or|greater|param 4|division|const 5.163362|const -9.068711|param 7|param 3|not|const -8.850357|param 7|param 3|greater|param 4|subtract|division|if|const 1.129728|param 8|param 9|xnor|xnor|add|xnor|division|param 5|param 9|param 11|equal|multiply|const 2.288346|param 11|equal|const 6.006185|const -4.795335|const 6.858113|param 7|not|and|param 11|param 1|subtract|xor|const -6.007191|division|const -9.695269|not|equal|add|if|division|neg|const -6.796291|subtract|xor|const -8.183680|const 0.352701|param 10|const -5.574947|param 3|abs|const -0.355154|const 5.235267|const 3.266637|param 3|division|param 7|add|add|const 6.179917|param 7|const 0.444269|const -5.087925|division|const 8.393194|add|add|add|xnor|neg|equal|param 3|param 3|param 0|param 7|param 7|param 3|param 5|param 3|const 0.046529|const 0.046529|add|division|division|param 3|param 3|add|add|division|neg|const 0.918398|param 0|param 7|param 7|division|const 9.699248|add|param 7|param 3'
    # node, desc = desc_to_node(desc)
    # print simplify(node).desc

    # 验证node.length
    # node = random_tree(20, 12, 0.7, 0.7)
    # print node.desc
    # print node.length()

    # 验证desc_to_node
    # node, desc = desc_to_node('equal|abs|xnor|not|if|multiply|division|xor|param 8|param 2|subtract|param 7|param 7|const 84|add|param 1|param 5|param 10|param 10|add|not|xnor|greater|const 76|param 3|greater|const 34|param 5|neg|mod|multiply|param 0|xor|const 8|mod|const 42|param 6|param 10')
    # show_node(node)

    # 验证pick_node
    # nodes = []
    # for i in range(0, 20):
    #     nodes.append(random_tree(5, 12, 0.7, 0.7))
    # node = nodes[0]
    # print node.desc
    # partner = pick_node(nodes, node)
    # print partner.desc
    # print crossing(node, partner, 0.1).desc

    # 验证crossing
    # node1 = random_tree(5, 20, 0.7, 0.7)
    # node2 = random_tree(5, 20, 0.7, 0.7)
    # show_node(node1)
    # show_node(node2)
    # print 'crossing'
    # if len(node1.children) > 0 and len(node2.children) > 0:
    #     show_node(crossing(node1, node2, 0.1))

