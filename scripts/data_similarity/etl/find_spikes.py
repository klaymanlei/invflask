# coding:utf-8

import logging
import datetime
import random
import time

from scripts.data_similarity.st.spike_terms.dao.db_utils import *
from scripts.data_similarity.st.spike_terms.dao.Record import Record

db_path = '../../../data/data_similarity/'
db_name = 'data_similarity.db'
code = 'stcxA'
term = 5
db = DaoDbUtils(db_path)


def set_log_level(level):
    fmt_str = '%(asctime)s %(name)s(%(lineno)d) %(levelname)s %(message)s'
    if level == 'DEBUG':
        logging.basicConfig(level=logging.DEBUG, format=fmt_str)
    elif level == 'INFO':
        logging.basicConfig(level=logging.INFO, format=fmt_str)


def setup(hist):
    init_data('%s%s.csv' % (db_path, code), hist)


def init_data(path, hist, end_date='2022-08-04'):
    last = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
    input = open(path, 'r')
    for line in input:
        data = line.split(',')
        date = datetime.datetime.strptime(data[1], '/' in data[1] and '%Y/%m/%d' or '%Y-%m-%d').date()
        r = Record(data[0], date, float(data[5]), float(data[3]), float(data[4]), float(data[2]), long(data[10]))
        if date <= last and len(data) > 11 and float(data[2]) > 0:
            hist.append(r)
    input.close()
    hist.reverse()


def gen_spikes(avgs, spikes, dur):
    for avg in avgs:
        type = 'mv%d' % dur
        cur_id = '%s_spike_%s_%s_%s' % (code, type, avg[0], gen_rand())
        spike = Spike(cur_id, code, avg[0], type, '0', avg[1])

        spikes.append(spike)
        if len(spikes) >= 3:
            last_spike = spikes[-2]
            last_2_spike = spikes[-3]
            cur_diff = spike.value - last_spike.value
            last_diff = last_spike.value - last_2_spike.value
            if last_diff > 0 and cur_diff < 0:
                last_spike.direction = '1'
            elif last_diff < 0 and cur_diff > 0:
                last_spike.direction = '-1'


def gen_rand():
    p2 = round(random.random(), 7)
    p1 = round(time.time() % 1000000, 0)
    return format(p1 + p2, '10.5f')


def spike_filter(hist, value):
    if len(hist) < 2:
        return False
    return (hist[-2][1] - value) * (value - hist[-1][1]) >= 0


def new_spike(code, hist, date, value, dur):
    type = 'mv%d' % dur
    cur_id = '%s_spike_%s_%s_%s' % (code, type, date, gen_rand())
    if hist is None or len(hist) == 0:
        return None, Spike(cur_id, code, date, type, '0', value)
    else:
        if spike_filter(hist, value):
            hist[-1].date = date
            return hist[-1], None
        elif len(hist) == 1:
            return hist[-1], Spike(cur_id, code, date, type, '0', value)
        spike_1 = hist[-1]
        i = 2
        spike_2 = hist[-i]
        while spike_2.value == spike_1.value and i < len(hist):
            i += 1
            spike_2 = hist[-i]
        if spike_1.value > value and spike_1.value > spike_2.value:
            spike_1.direction = '1'
        elif spike_1.value < value and spike_1.value < spike_2.value:
            spike_1.direction = '-1'
        return spike_1, Spike(cur_id, code, date, type, '0', value)


def filter_avgs(avgs):
    i = 2
    while i < len(avgs):
        if spike_filter(avgs[:i], avgs[i][1]):
            avg = avgs.pop(i)
            avgs[i - 1] = (avg[0], avgs[i - 1][1])
        else:
            i += 1


if __name__ == '__main__':
    set_log_level('INFO')
    hist = []
    setup(hist)

    avgs = []
    sum = 0
    for i in range(0, len(hist)):
        sum += hist[i].col4
        if i >= term:
            sum -= hist[i - term].col4
        if i >= (term - 1):
            avgs.append((hist[i].date, float(sum) / term))

    # for avg in avgs:
    #     print avg[0], avg[1]

    filter_avgs(avgs)

    # for avg in avgs:
    #     print avg[0], avg[1]


    spikes = []
    gen_spikes(avgs, spikes, term)

    db.save_spikes(spikes)
