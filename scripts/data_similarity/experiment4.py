# coding:utf-8

import sys
import datetime
import time
import random

from scripts.data_similarity.st.spike_terms.dao.Spike import Spike
from scripts.data_similarity.st.spike_terms.dao.Record import Record
from scripts.data_similarity.st.spike_terms.sqlite_utils import SqliteUtil

all_data = []
data_map = {}
db = SqliteUtil()


def init(end_date, path):
    last = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
    input = open(path, 'r')
    for line in input:
        data = line.split(',')
        date = datetime.datetime.strptime(data[1], '/' in data[1] and '%Y/%m/%d' or '%Y-%m-%d').date()
        r = Record(data[0], date, float(data[5]), float(data[3]), float(data[4]), float(data[2]), long(data[10]))
        if date <= last and len(data) > 11 and float(data[2]) > 0:
            all_data.append(r)
        if len(data) > 11 and float(data[2]) > 0:
            data_map[date.strftime('%Y-%m-%d')] = r
    input.close()
    all_data.reverse()


def load_hist(code, dur):
    return all_data


def load_cur(code, target_date):
    # load from map
    r = target_date in data_map and data_map[target_date] or None
    return r


def load_spike(code, type, date):
    spikes = []

    # load spikes from db
    sql = 'select id, code, date, type, direction, value from spikes where code=\'%s\' and type=\'mv%s\' and date < \'%s\'' % (
        code, type, date)
    rs = db.select(sql)
    for record in rs:
        spike = Spike(record[0].encode('utf-8'), record[1].encode('utf-8'), record[2].encode('utf-8'),
                      record[3].encode('utf-8'), record[4].encode('utf-8'), record[5])
        spikes.append(spike)

    return spikes


def gen_rand():
    p2 = round(random.random(), 7)
    p1 = round(time.time() % 1000000, 0)
    return format(p1 + p2, '10.5f')


def spike_filter(hist, value):
    if len(hist) < 2:
        return False
    return value / hist[-1].value < 1.02 and value / hist[-1].value > 0.98
    # return (hist[-2].value - value) * (value - hist[-1].value) > 0


def new_spike(code, hist, date, value, dur):
    type = 'mv%d' % dur
    cur_id = '%s_spike_%s_%s_%s' % (code, type, date, gen_rand())
    if hist is None or len(hist) == 0:
        return None, Spike(cur_id, code, date, type, '0', value)
    else:
        if spike_filter(hist, value):
            hist[-1].date = date
            return hist[-1], None
        elif len(hist) == 1:
            return hist[-1], Spike(cur_id, code, date, type, '0', value)
        spike_1 = hist[-1]
        i = 2
        spike_2 = hist[-i]
        while spike_2.value == spike_1.value and i < len(hist):
            i += 1
            spike_2 = hist[-i]
        if spike_1.value > value and spike_1.value > spike_2.value:
            spike_1.direction = '1'
        elif spike_1.value < value and spike_1.value < spike_2.value:
            spike_1.direction = '-1'
        return spike_1, Spike(cur_id, code, date, type, '0', value)


def save_spikes(spikes):
    # save to db
    for spike in spikes:
        del_sql = 'delete from spikes where code=\'%s\' and date>=\'%s\' and type=\'%s\'' % (
            spike.code, spike.date, spike.type)
        db.update(del_sql)
        insert_sql = 'insert into spikes (id, code, date, type, direction, value) values (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%f\')' % (
            spike.id, spike.code, spike.date, spike.type, spike.direction, spike.value)
        db.update(insert_sql)
    db.commit()


def update_spikes(spike):
    # save to db
    insert_sql = 'update spikes set date=\'%s\' where id=\'%s\'' % (
        spike.date, spike.id)
    db.update(insert_sql)
    db.commit()


# def load_term(code, dur, date):
#     terms = []
#     type = 'mv%s' % dur
#
#     sql = 'select name, code, type, spike_id, spike_date, spike_direction, value from terms where code=\'%s\' and type=\'%s\' and spike_date < \'%s\'' % (
#         code, type, date)
#     rs = db.select(sql)
#     term = None
#     for record in rs:
#         name = record[0].encode('utf-8')
#         if term is None or term.name != name:
#             term = Term()
#             term.name = name
#             term.code = code
#             term.type = type
#             term.direct = 'unknown'
#             term.spikes = []
#             terms.append(term)
#         spike = Spike(record[3].encode('utf-8'), code, record[4].encode('utf-8'), type, record[5].encode('utf-8'), record[6])
#         term.append(spike)
#
#     return terms


# def new_term(spike):
#     new_one = Term()
#     new_one.code = spike.code
#     new_one.name = '%s_term_%s_%s' % (spike.code, spike.type, spike.date)
#     new_one.type = spike.type
#     new_one.direct = 'unknown'
#     new_one.append(spike)
#     return new_one


# def create_or_update_term(terms, spike):
#     term = terms[-1]
#     # if spike is not None and spike.date == '2021-09-22':
#     #     print spike.date
#     if term.direct == 'unknown':
#         term.append(spike)
#         if term.direct == 'unknown':
#             return None
#         else:
#             return term
#     elif term.direct != '0' and int(term.direct) * (spike.value - term.spikes[-2].value) > 0:
#         term.append(spike)
#         return None
#     elif term.direct == '0' and (term.spikes[-1].value - term.spikes[-3].value) * (spike.value - term.spikes[-2].value) < 0:
#         term.append(spike)
#         return None
#     else:
#         new_one = new_term(term.spikes[-2])
#         new_one.append(term.spikes[-1])
#         new_one.append(spike)
#         terms.append(new_one)
#         return new_one


# def save_term(spike, term):
#     del_sql = 'delete from terms where code=\'%s\' and spike_date>=\'%s\' and type=\'%s\' and name>=\'%s\'' % (
#         term.code, spike.date, term.type, term.name)
#     db.update(del_sql)
#     insert_sql = 'insert into terms (name, code, type, spike_id, spike_date, spike_direction, value) values (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%f\')' % (
#         term.name, term.code, term.type, spike.id, spike.date, spike.direction, spike.value)
#     db.update(insert_sql)
#     db.commit()


def data_process(code, cur_data, term_dur, avg, hist_spikes):
    last_spike, cur_spike = new_spike(code, hist_spikes, cur_data.date, avg, term_dur)
    if cur_spike is not None:
        hist_spikes.append(cur_spike)
        if last_spike is not None and last_spike.direction in ('-1', '1'):
            return last_spike
    return None


# def show_hint(spikes, all_in):
#     if all_in == 'FIN' and spikes[-1].value < spikes[-2].value:
#         cur_dif = spikes[-2].value - spikes[-1].value
#         last_dif = spikes[-2].value - spikes[-3].value
#         if cur_dif < last_dif * 0.5:
#             return 'OK'
#     elif all_in == 'OK':
#         if spikes[-1].value < spikes[-2].value:
#             cur_dif = spikes[-2].value - spikes[-1].value
#             last_dif = spikes[-2].value - spikes[-3].value
#             if cur_dif >= last_dif * 0.5:
#                 return 'FIN_pending'
#         elif spikes[-1].value > spikes[-2].value:
#             cur_dif = spikes[-1].value - spikes[-2].value
#             last_dif = spikes[-3].value - spikes[-2].value
#             if cur_dif < last_dif * 2:
#                 return 'FIN'
#     return all_in

def show_hint(spikes, all_in):
    if all_in == 'FIN' and spikes[-1].direction == '-1':
        return 'OK'
    else:
        return 'FIN'

def avg(cur, hist, dur):
    sum = cur.col4
    for i in range(1, dur):
        sum += hist[-i].col4
    return float(sum / dur)


def append(hist, code, cur_data, term_dur, all_in, spikes):
    real_spikes = []
    for spike in spikes:
        if spike.direction in ('-1', '1'):
            real_spikes.append(spike)
    term_avg = avg(cur_data, hist, term_dur)
    # if cur_data.date.strftime('%Y-%m-%d') == '2011-02-28':
    #     print cur_data
    new_spike = data_process(code, cur_data, term_dur, term_avg, spikes)
    if new_spike is not None:
        real_spikes.append(new_spike)
        # print new_spike

    if all_in == 'FIN_pending' and new_spike:
        return 'FIN'
    elif len(real_spikes) > 2 and new_spike:
        return show_hint(real_spikes, all_in)
    else:
        return all_in


# 仅spike
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Usage:', sys.argv[0], 'code term'
        exit(1)
    code = sys.argv[1]
    term_dur = sys.argv[2]

    data_path = '../../data/data_similarity/%s.csv' % code

    start_date = datetime.datetime.strptime('2010-10-01', '%Y-%m-%d').date()
    end_date = datetime.datetime.strptime('2022-06-30', '%Y-%m-%d').date()
    init(start_date.strftime('%Y-%m-%d'), data_path)

    cur_date = start_date + datetime.timedelta(days=1)

    hist_data = load_hist(code, term_dur)
    spikes = []
    all_in = 'FIN'
    last_flag = 'FIN'
    while cur_date <= end_date:
        cur_data = load_cur(code, cur_date.strftime('%Y-%m-%d'))
        if cur_data is not None:
            # print cur_data.col4
            if last_flag != all_in and all_in in ['OK', 'FIN']:
                print all_in, spikes[-2].date, spikes[-2].value, cur_data.date, cur_data.col1
            last_flag = all_in
            all_in = append(hist_data, code, cur_data, int(term_dur), all_in, spikes)

            hist_data.append(cur_data)
            # print cur_data.date
        cur_date = cur_date + datetime.timedelta(days=1)
    # for spike in spikes:
    #     if spike.direction in ['1', '-1']:
    #         print spike.date, spike.value