#coding:utf-8

import random
import numpy as np

step = 0.1

def fake(start, end, n):
    values = []
    if n <= 2:
        values.append(start)
        values.append(end)
        return values

    wave = float(pow(1.005, n / 20) - 1 + 0.2)

    mid = n / 2
    max_value = round(min(start * pow(step + 1, mid - 0), end * pow(step + 1, n - 1 - mid)), 2)
    min_value = round(max(start * pow(1 - step, mid - 0), end * pow(1 - step, n - 1 - mid)), 2)
    mid_value = round(float(end - start) / 2 + start)

    rand = -1
    while rand >= 1 or rand <= -1:
        rand = np.random.normal(loc=0, scale=wave, size=None)
    value = rand * min(max_value - mid_value, mid_value - min_value) + mid_value

    # print 'wave: %f, start: %f, end: %f, max: %f, min: %f, mid: %f, n: %d' % (wave, start, end, max_value, min_value, value, n)

    part_1 = fake(start, value, mid + 1)
    for i in part_1:
        values.append(round(i, 2))

    part_2 = fake(value, end, n - mid)
    for i in part_2[1:]:
        values.append(round(i, 2))
    return values

if __name__ == '__main__':
    values = fake(30, 300, 2049)
    for i in values[1:-1]:
        print i

