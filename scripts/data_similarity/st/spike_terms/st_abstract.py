# coding:utf-8

import datetime

from scripts.data_similarity.st.spike_terms.dao.Record import Record
from scripts.data_similarity.st.spike_terms.dao.db_utils import DaoDbUtils
from st_utils import *


class StAbstract(object):
    all_data = []
    data_map = {}
    dao_utils = None
    start_date = datetime.datetime.strptime('2010-08-31', '%Y-%m-%d').date()
    end_date = datetime.datetime.strptime('2022-07-30', '%Y-%m-%d').date()

    def __init__(self, path):
        self.dao_utils = DaoDbUtils(path)
        pass

    def init_data(self, end_date, path):
        last = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
        input = open(path, 'r')
        for line in input:
            data = line.split(',')
            date = datetime.datetime.strptime(data[1], '/' in data[1] and '%Y/%m/%d' or '%Y-%m-%d').date()
            r = Record(data[0], date, float(data[5]), float(data[3]), float(data[4]), float(data[2]), long(data[10]))
            if date <= last and len(data) > 11 and float(data[2]) > 0:
                self.all_data.append(r)
            if len(data) > 11 and float(data[2]) > 0:
                self.data_map[date.strftime('%Y-%m-%d')] = r
        input.close()
        self.all_data.reverse()

    def load_hist(self, code, dur):
        return self.all_data

    def load_cur(self, code, target_date):
        # load from map
        r = target_date in self.data_map and self.data_map[target_date] or None
        return r

    def append(self, hist, code, cur_data, all_in):
        pass

    def go_through(self, code, data_path, params):
        pass
