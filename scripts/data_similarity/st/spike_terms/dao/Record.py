#coding:utf-8

class Record(object):
    code = None
    date = None
    col1 = None
    col2 = None
    col3 = None
    col4 = None
    col5 = None

    def __init__(self, code, date, col1, col2, col3, col4, col5):
        self.code = code
        self.date = date
        self.col1 = col1
        self.col2 = col2
        self.col3 = col3
        self.col4 = col4
        self.col5 = col5
