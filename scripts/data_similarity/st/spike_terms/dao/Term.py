#coding:utf-8

class Term(object):
    code = None
    name = None
    type = None
    direct = None
    spikes = []
    high = None
    low = None
    start = None
    end = None

    def __init__(self):
        self.code = None
        self.name = None
        self.type = None
        self.direct = None
        self.spikes = []
        self.high = None
        self.low = None
        self.start = None
        self.end = None

    def __len__(self):
        return len(self.spikes)

    def append(self, spike):
        self.spikes.append(spike)
        if self.high is None or spike.value > self.high.value:
            self.high = spike
        if self.low is None or spike.value < self.low.value:
            self.low = spike
        if self.start is None or spike.date < self.start:
            self.start = spike.date
        if self.end is None or spike.date > self.end:
            self.end = spike.date
        if len(self.spikes) >= 4 and self.direct == 'unknown':
            diff1 = self.spikes[2].value - self.spikes[0].value
            diff2 = self.spikes[3].value - self.spikes[1].value
            if diff1 < 0 and diff2 < 0:
                self.direct = '-1'
            elif diff1 > 0 and diff2 > 0:
                self.direct = '1'
            else:
                self.direct = '0'

    def __repr__(self):
        return 'term\t%s\t%s\t%s\t%s' % (self.name, self.code, self.type, self.direct)
