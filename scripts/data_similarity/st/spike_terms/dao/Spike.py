#coding:utf-8

class Spike(object):
    id = None
    code = None
    date = None
    type = None
    direction = None
    value = None

    def __init__(self, id, code, date, type, direction, value):
        self.id = id
        self.code = code
        self.date = date
        self.type = type
        self.direction = direction
        self.value = value

    def __repr__(self):
        return 'spike\t%s\t%s\t%s\t%s\t%f' % (self.code, self.date, self.type, self.direction, self.value)
