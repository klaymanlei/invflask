# coding:utf-8

from Spike import Spike
from Term import Term
from scripts.data_similarity.st.spike_terms.sqlite_utils import SqliteUtil


class DaoDbUtils(object):
    db = None

    def __init__(self, path):
        self.db = SqliteUtil(path)

    def load_term(self, code, dur, date):
        terms = []
        type = 'mv%s' % dur

        sql = 'select name, code, type, spike_id, spike_date, spike_direction, value from terms where code=\'%s\' and type=\'%s\' and spike_date < \'%s\'' % (
            code, type, date)
        rs = self.db.select(sql)
        term = None
        for record in rs:
            name = record[0].encode('utf-8')
            if term is None or term.name != name:
                term = Term()
                term.name = name
                term.code = code
                term.type = type
                term.direct = 'unknown'
                term.spikes = []
                terms.append(term)
            spike = Spike(record[3].encode('utf-8'), code, record[4].encode('utf-8'), type, record[5].encode('utf-8'),
                          record[6])
            term.append(spike)

        return terms

    def update_spikes(self, spike):
        # save to db
        insert_sql = 'update spikes set date=\'%s\' where id=\'%s\'' % (
            spike.date, spike.id)
        self.db.update(insert_sql)
        self.db.commit()

    def save_spikes(self, spikes):
        # save to db
        for spike in spikes:
            del_sql = 'delete from spikes where code=\'%s\' and date>=\'%s\' and type=\'%s\'' % (
                spike.code, spike.date, spike.type)
            self.db.update(del_sql)
            insert_sql = 'insert into spikes (id, code, date, type, direction, value) values (\'%s\', \'%s\', \'%s\'' \
                         ', \'%s\', \'%s\', \'%f\')' % (
                             spike.id, spike.code, spike.date, spike.type, spike.direction, spike.value)
            self.db.update(insert_sql)
        self.db.commit()

    def save_term(self, spike, term):
        del_sql = 'delete from terms where code=\'%s\' and spike_date>=\'%s\' and type=\'%s\' and name>=\'%s\'' % (
            term.code, spike.date, term.type, term.name)
        self.db.update(del_sql)
        insert_sql = 'insert into terms (name, code, type, spike_id, spike_date, spike_direction, value) values (' \
                     '\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%f\')' % (
                         term.name, term.code, term.type, spike.id, spike.date, spike.direction, spike.value)
        self.db.update(insert_sql)
        self.db.commit()

    def load_spike(self, code, type, before):
        spikes = []

        # load spikes from db
        sql = 'select id, code, date, type, direction, value from spikes where code=\'%s\' and type=\'mv%s\' ' \
              'and date < \'%s\'' % (
                  code, type, before)
        rs = self.db.select(sql)
        for record in rs:
            spike = Spike(record[0].encode('utf-8'), record[1].encode('utf-8'), record[2].encode('utf-8'),
                          record[3].encode('utf-8'), record[4].encode('utf-8'), record[5])
            spikes.append(spike)

        return spikes


    def load_spike_between(self, code, type, start, end):
        spikes = []

        # load spikes from db
        sql = 'select id, code, date, type, direction, value from spikes where code=\'%s\' and type=\'mv%s\' ' \
              'and date between \'%s\' and \'%s\' order by date' % (
                  code, type, start, end)
        rs = self.db.select(sql)
        for record in rs:
            spike = Spike(record[0].encode('utf-8'), record[1].encode('utf-8'), record[2].encode('utf-8'),
                          record[3].encode('utf-8'), record[4].encode('utf-8'), record[5])
            spikes.append(spike)

        return spikes
