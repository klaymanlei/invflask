# coding:utf-8

import datetime
import time
import random

from scripts.data_similarity.st.spike_terms.dao.Term import Term
from scripts.data_similarity.st.spike_terms.dao.Spike import Spike
from scripts.data_similarity.st.spike_terms.dao.Record import Record
from scripts.data_similarity.st.spike_terms.sqlite_utils import SqliteUtil


# 综合考虑长短周期
class StObj1(object):
    all_data = []
    data_map = {}
    db = None

    def __init__(self, path):
        self.db = SqliteUtil(path)
        pass

    def init_data(self, end_date, path):
        last = datetime.datetime.strptime(end_date, '%Y-%m-%d').date()
        input = open(path, 'r')
        for line in input:
            data = line.split(',')
            date = datetime.datetime.strptime(data[1], '/' in data[1] and '%Y/%m/%d' or '%Y-%m-%d').date()
            r = Record(data[0], date, float(data[5]), float(data[3]), float(data[4]), float(data[2]), long(data[10]))
            if date <= last and len(data) > 11 and float(data[2]) > 0:
                self.all_data.append(r)
            if len(data) > 11 and float(data[2]) > 0:
                self.data_map[date.strftime('%Y-%m-%d')] = r
        input.close()
        self.all_data.reverse()

    def load_hist(self, code, dur):
        return self.all_data

    def load_cur(self, code, target_date):
        # load from map
        r = target_date in self.data_map and self.data_map[target_date] or None
        return r

    def load_spike(self, code, type, date):
        spikes = []

        # load spikes from db
        sql = 'select id, code, date, type, direction, value from spikes where code=\'%s\' and type=\'mv%s\' and date < \'%s\'' % (
            code, type, date)
        rs = self.db.select(sql)
        for record in rs:
            spike = Spike(record[0].encode('utf-8'), record[1].encode('utf-8'), record[2].encode('utf-8'),
                          record[3].encode('utf-8'), record[4].encode('utf-8'), record[5])
            spikes.append(spike)

        return spikes

    def gen_rand(self):
        p2 = round(random.random(), 7)
        p1 = round(time.time() % 1000000, 0)
        return format(p1 + p2, '10.5f')

    def spike_filter(self, hist, value):
        if len(hist) < 2:
            return False
        return (hist[-2].value - value) * (value - hist[-1].value) >= 0

    def new_spike(self, code, hist, date, value, dur):
        type = 'mv%d' % dur
        cur_id = '%s_spike_%s_%s_%s' % (code, type, date, self.gen_rand())
        if hist is None or len(hist) == 0:
            return None, Spike(cur_id, code, date, type, '0', value)
        else:
            if self.spike_filter(hist, value):
                hist[-1].date = date
                return hist[-1], None
            elif len(hist) == 1:
                return hist[-1], Spike(cur_id, code, date, type, '0', value)
            spike_1 = hist[-1]
            i = 2
            spike_2 = hist[-i]
            while spike_2.value == spike_1.value and i < len(hist):
                i += 1
                spike_2 = hist[-i]
            if spike_1.value > value and spike_1.value > spike_2.value:
                spike_1.direction = '1'
            elif spike_1.value < value and spike_1.value < spike_2.value:
                spike_1.direction = '-1'
            return spike_1, Spike(cur_id, code, date, type, '0', value)

    def save_spikes(self, spikes):
        # save to db
        for spike in spikes:
            del_sql = 'delete from spikes where code=\'%s\' and date>=\'%s\' and type=\'%s\'' % (
                spike.code, spike.date, spike.type)
            self.db.update(del_sql)
            insert_sql = 'insert into spikes (id, code, date, type, direction, value) values (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%f\')' % (
                spike.id, spike.code, spike.date, spike.type, spike.direction, spike.value)
            self.db.update(insert_sql)
        self.db.commit()

    def update_spikes(self, spike):
        # save to db
        insert_sql = 'update spikes set date=\'%s\' where id=\'%s\'' % (
            spike.date, spike.id)
        self.db.update(insert_sql)
        self.db.commit()

    def load_term(self, code, dur, date):
        terms = []
        type = 'mv%s' % dur

        sql = 'select name, code, type, spike_id, spike_date, spike_direction, value from terms where code=\'%s\' and type=\'%s\' and spike_date < \'%s\'' % (
            code, type, date)
        rs = self.db.select(sql)
        term = None
        for record in rs:
            name = record[0].encode('utf-8')
            if term is None or term.name != name:
                term = Term()
                term.name = name
                term.code = code
                term.type = type
                term.direct = 'unknown'
                term.spikes = []
                terms.append(term)
            spike = Spike(record[3].encode('utf-8'), code, record[4].encode('utf-8'), type, record[5].encode('utf-8'),
                          record[6])
            term.append(spike)

        return terms

    def new_term(self, spike):
        new_one = Term()
        new_one.code = spike.code
        new_one.name = '%s_term_%s_%s' % (spike.code, spike.type, spike.date)
        new_one.type = spike.type
        new_one.direct = 'unknown'
        new_one.append(spike)
        return new_one

    def create_or_update_term(self, terms, spike):
        term = terms[-1]
        # if spike is not None and spike.date == '2021-09-22':
        #     print spike.date
        if term.direct == 'unknown':
            term.append(spike)
            if term.direct == 'unknown':
                return None
            else:
                return term
        elif term.direct != '0' and int(term.direct) * (spike.value - term.spikes[-2].value) > 0:
            term.append(spike)
            return None
        elif term.direct == '0' and (term.spikes[-1].value - term.spikes[-3].value) * (
                spike.value - term.spikes[-2].value) < 0:
            term.append(spike)
            return None
        else:
            new_one = self.new_term(term.spikes[-2])
            new_one.append(term.spikes[-1])
            new_one.append(spike)
            terms.append(new_one)
            return new_one

    def save_term(self, spike, term):
        del_sql = 'delete from terms where code=\'%s\' and spike_date>=\'%s\' and type=\'%s\' and name>=\'%s\'' % (
            term.code, spike.date, term.type, term.name)
        self.db.update(del_sql)
        insert_sql = 'insert into terms (name, code, type, spike_id, spike_date, spike_direction, value) values (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%f\')' % (
            term.name, term.code, term.type, spike.id, spike.date, spike.direction, spike.value)
        self.db.update(insert_sql)
        self.db.commit()

    def data_process(self, code, cur_data, term_dur, avg, hist_spikes, hist_term):
        # hist_spikes = load_spike(code, term_dur, cur_data.date)
        last_spike, cur_spike = self.new_spike(code, hist_spikes, cur_data.date, avg, term_dur)
        if cur_spike is not None:
            hist_spikes.append(cur_spike)
        # hist_term = []
        new_spike_flag = False
        # if last_spike is not None:
        #     hist_term = load_term(code, term_dur, last_spike.date)
        if last_spike is not None and cur_spike is None:
            # update_spikes(last_spike)
            return False, False, len(hist_term) > 0 and hist_term[-1] or None
        # if last_spike is not None and last_spike.direction != '0':
        #     save_spikes([last_spike, cur_spike])
        # else:
        #     save_spikes([cur_spike])
        if last_spike is not None and last_spike.direction in ('-1', '1'):
            new_spike_flag = True
            # print last_spike
            if len(hist_term) == 0:
                cur_term = self.new_term(last_spike)
                # save_term(last_spike, cur_term)
                hist_term.append(cur_term)
                return True, False, None

            cur_term = self.create_or_update_term(hist_term, last_spike)
            if cur_term is not None:
                # for spike in cur_term.spikes:
                #     save_term(spike, cur_term)
                return True, True, cur_term
            # else:
            #     save_term(last_spike, hist_term[-1])
        return new_spike_flag, False, len(hist_term) > 0 and hist_term[-1] or None

    def show_hint(self, short_terms, long_term, all_in):
        short_term = short_terms[-1]
        if all_in == 'FIN' and long_term.direct == '1' and short_term.direct == '1':
            if short_term.spikes[-1].value < short_term.spikes[-2].value:
                return 'OK'
            else:
                return 'OK_pending'
        elif all_in == 'FIN' and long_term.direct == '1' and short_term.direct == 'unknown':
            if len(short_terms) >= 2 and short_terms[-2].direct == '-1':
                if short_term.spikes[-1].value < short_term.spikes[-2].value:
                    return 'OK'
                else:
                    return 'OK_pending'
        elif all_in == 'OK' and (long_term.direct != '1' or short_term.direct == '-1'):
            # elif all_in == 'OK' and (long_term.direct != '1' or short_term.direct != '1'):
            if short_term.spikes[-1].value > short_term.spikes[-2].value:
                return 'FIN'
            else:
                return 'FIN_pending'
        return all_in

    def avg(self, cur, hist, dur):
        sum = cur.col4
        for i in range(1, dur):
            sum += hist[-i].col4
        return float(sum / dur)

    def append(self, hist, code, cur_data, short_term_dur, long_term_dur, all_in, long_spikes, long_terms, short_spikes,
               short_terms):
        long_term_avg = self.avg(cur_data, hist, long_term_dur)
        short_term_avg = self.avg(cur_data, hist, short_term_dur)
        # if cur_data.date.strftime('%Y-%m-%d') == '2016-04-12':
        #     print cur_data
        new_long_spike, new_long_term, cur_long_term = self.data_process(code, cur_data, long_term_dur, long_term_avg,
                                                                         long_spikes, long_terms)
        new_short_spike, new_short_term, cur_short_term = self.data_process(code, cur_data, short_term_dur,
                                                                            short_term_avg, short_spikes, short_terms)

        # if new_long_term:
        #     print 'long_term', cur_data.date, cur_long_term
        # if new_short_term:
        #     print 'short_term', cur_data.date, cur_short_term

        if all_in == 'OK_pending' and new_short_spike:
            if cur_short_term.direct == '1' and cur_long_term.direct == '1':
                return 'OK'
            else:
                return 'FIN'
        elif all_in == 'FIN_pending' and new_short_spike:
            return 'FIN'
        elif cur_long_term is not None and new_short_term:
            return self.show_hint(short_terms, cur_long_term, all_in)
        else:
            return all_in

    def go_through(self, code, data_path, params):
        short_term_dur = params[0]
        long_term_dur = params[1]

        # data_path = '../../data/data_similarity/%s.csv' % code

        start_date = datetime.datetime.strptime('2010-08-31', '%Y-%m-%d').date()
        end_date = datetime.datetime.strptime('2022-07-30', '%Y-%m-%d').date()
        self.init_data(start_date.strftime('%Y-%m-%d'), data_path)

        cur_date = start_date + datetime.timedelta(days=1)

        hist = self.load_hist(code, long_term_dur)

        long_spikes = self.load_spike(code, long_term_dur, start_date)
        short_spikes = self.load_spike(code, short_term_dur, start_date)

        long_terms = self.load_term(code, long_term_dur, start_date)
        short_terms = self.load_term(code, short_term_dur, start_date)

        all_in = 'FIN'
        last_flag = 'FIN'
        while cur_date <= end_date:
            cur_data = self.load_cur(code, cur_date.strftime('%Y-%m-%d'))
            if cur_data is not None:
                # print cur_data.col4
                if last_flag != all_in and all_in in ['OK', 'FIN'] and not (
                        last_flag == 'OK_pending' and all_in == 'FIN'):
                    if short_spikes[-1].value < long_spikes[-1].value and all_in == 'OK':
                        all_in = 'FIN'
                    else:
                        print code, all_in, cur_data.date, cur_data.col1
                last_flag = all_in
                if len(hist) >= int(long_term_dur) - 1:
                    all_in = self.append(hist, code, cur_data, int(short_term_dur), int(long_term_dur), all_in,
                                         long_spikes, long_terms, short_spikes, short_terms)
                hist.append(cur_data)
                # print cur_data.date
            cur_date = cur_date + datetime.timedelta(days=1)
