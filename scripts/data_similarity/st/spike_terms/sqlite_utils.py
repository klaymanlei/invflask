import sqlite3

import os

class SqliteUtil(object):

    db_path = '../../../../data/data_similarity/'
    db_name = 'data_similarity.db'

    conn = None

    def __init__(self, path=None, name=None):
        if path is not None:
            self.db_path = path
        if name is not None:
            self.db_name = name
        self.conn = sqlite3.connect(self.db_path + self.db_name)

    def update(self, sql):
        cursor = self.conn.cursor()
        cursor.execute(sql)
        cursor.close()
        # self.conn.commit()

    def select(self, sql):
        cursor = self.conn.cursor()
        cursor.execute(sql)
        rs = cursor.fetchall()
        cursor.close()
        return rs

    def commit(self):
        self.conn.commit()

    def close(self):
        self.conn.close()

if __name__ == '__main__':
    sl = SqliteUtil('../../../../data/data_similarity/data_similarity.db')
    sl.close()