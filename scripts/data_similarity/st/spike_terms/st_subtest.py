# coding:utf-8

import datetime

from st_abstract import StAbstract
from st_utils import *


class StSubTest(StAbstract):
    short_term_dur = '5'
    long_term_dur = '20'
    long_spikes = []
    long_terms = []
    short_spikes = []
    short_terms = []

    def __init__(self, path):
        StAbstract.__init__(self, path)

    def data_process(self, code, cur_data, term_dur, avg, hist_spikes, hist_term):
        last_spike, cur_spike = new_spike(code, term_dur, cur_data.date, avg,
                                          len(hist_spikes) >= 1 and hist_spikes[-1] or None,
                                          len(hist_spikes) >= 2 and hist_spikes[-2] or None)
        if cur_spike is not None:
            hist_spikes.append(cur_spike)
        new_spike_flag = False
        if last_spike is not None and cur_spike is None:
            # update_spikes(last_spike)
            return False, False, len(hist_term) > 0 and hist_term[-1] or None
        # if last_spike is not None and last_spike.direction != '0':
        #     save_spikes([last_spike, cur_spike])
        # else:
        #     save_spikes([cur_spike])
        if last_spike is not None and last_spike.direction in ('-1', '1'):
            new_spike_flag = True
            # print last_spike
            if len(hist_term) == 0:
                cur_term = new_term(last_spike)
                # save_term(last_spike, cur_term)
                hist_term.append(cur_term)
                return True, False, None

            cur_term = create_or_update_term(hist_term, last_spike)
            if cur_term is not None:
                # for spike in cur_term.spikes:
                #     save_term(spike, cur_term)
                return True, True, cur_term
            # else:
            #     save_term(last_spike, hist_term[-1])
        return new_spike_flag, False, len(hist_term) > 0 and hist_term[-1] or None

    def show_hint(self, long_term, all_in):
        short_term = self.short_terms[-1]
        if all_in == 'FIN' and long_term.direct == '1' and short_term.direct == '1':
            if short_term.spikes[-1].value < short_term.spikes[-2].value:
                return 'OK'
            else:
                return 'OK_pending'
        elif all_in == 'FIN' and long_term.direct == '1' and short_term.direct == 'unknown':
            if len(self.short_terms) >= 2 and self.short_terms[-2].direct == '-1':
                if short_term.spikes[-1].value < short_term.spikes[-2].value:
                    return 'OK'
                else:
                    return 'OK_pending'
        elif all_in == 'OK' and (long_term.direct != '1' or short_term.direct == '-1'):
        # elif all_in == 'OK' and (long_term.direct != '1' or short_term.direct != '1'):
            if short_term.spikes[-1].value > short_term.spikes[-2].value:
                return 'FIN'
            else:
                return 'FIN_pending'
        return all_in

    def append(self, hist, code, cur_data, all_in):
        long_term_avg = avg(cur_data, hist, self.long_term_dur)
        short_term_avg = avg(cur_data, hist, self.short_term_dur)
        # if cur_data.date.strftime('%Y-%m-%d') == '2016-04-12':
        #     print cur_data
        new_long_spike, new_long_term, cur_long_term = self.data_process(code, cur_data, self.long_term_dur, long_term_avg, self.long_spikes, self.long_terms)
        new_short_spike, new_short_term, cur_short_term = self.data_process(code, cur_data, self.short_term_dur, short_term_avg, self.short_spikes, self.short_terms)

        # if new_long_term:
        #     print 'long_term', cur_data.date, cur_long_term
        # if new_short_term:
        #     print 'short_term', cur_data.date, cur_short_term

        if all_in == 'OK_pending' and new_short_spike:
            if cur_short_term.direct == '1' and cur_long_term.direct == '1':
                return 'OK'
            else:
                return 'FIN'
        elif all_in == 'FIN_pending' and new_short_spike:
            return 'FIN'
        elif cur_long_term is not None and new_short_term:
            return self.show_hint(cur_long_term, all_in)
        else:
            return all_in

    def go_through(self, code, data_path, params):
        self.short_term_dur = params[0]
        self.long_term_dur = params[1]
        start = params[2]
        end = params[3]

        start_date = datetime.datetime.strptime(start, '%Y-%m-%d').date()
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d').date()
        self.init_data(start_date.strftime('%Y-%m-%d'), data_path)

        cur_date = start_date + datetime.timedelta(days=1)

        hist = self.load_hist(code, self.long_term_dur)

        self.long_spikes = self.dao_utils.load_spike(code, self.long_term_dur, start_date)
        self.short_spikes = self.dao_utils.load_spike(code, self.short_term_dur, start_date)

        self.long_terms = self.dao_utils.load_term(code, self.long_term_dur, start_date)
        self.short_terms = self.dao_utils.load_term(code, self.short_term_dur, start_date)

        all_in = 'FIN'
        last_flag = 'FIN'
        while cur_date <= end_date:
            cur_data = self.load_cur(code, cur_date.strftime('%Y-%m-%d'))
            if cur_data is not None:
                # print cur_data.col4
                if last_flag != all_in and all_in in ['OK', 'FIN'] and not (last_flag == 'OK_pending' and all_in == 'FIN'):
                    if self.short_spikes[-1].value < self.long_spikes[-1].value and all_in == 'OK':
                        all_in = 'FIN'
                    else:
                        print code, all_in, cur_data.date, cur_data.col1
                last_flag = all_in
                if len(hist) >= int(self.long_term_dur) - 1:
                    all_in = self.append(hist, code, cur_data, all_in)
                hist.append(cur_data)
                # print cur_data.date
            cur_date = cur_date + datetime.timedelta(days=1)
