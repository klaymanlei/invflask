# coding:utf-8

import random
import time
from scripts.data_similarity.st.spike_terms.dao.Spike import Spike
from scripts.data_similarity.st.spike_terms.dao.Term import Term


def gen_rand():
    p2 = round(random.random(), 7)
    p1 = round(time.time() % 1000000, 0)
    return format(p1 + p2, '10.5f')


def data_filter(value, value_1, value_2):
    return (value_2 - value) * (value - value_1) >= 0


def new_spike(code, dur, date, value, spike_1, spike_2):
    type = 'mv%d' % dur
    cur_id = '%s_spike_%s_%s_%s' % (code, type, date, gen_rand())
    if spike_1 is None and spike_2 is None:
        return None, Spike(cur_id, code, date, type, '0', value)
    else:
        if spike_1 is not None and spike_2 is not None and data_filter(value, spike_1.value, spike_2.value):
            spike_1.date = date
            return spike_1, None
        elif spike_1 is not None and spike_2 is None:
            return spike_1, Spike(cur_id, code, date, type, '0', value)
        if spike_1.value > value and spike_1.value > spike_2.value:
            spike_1.direction = '1'
        elif spike_1.value < value and spike_1.value < spike_2.value:
            spike_1.direction = '-1'
        return spike_1, Spike(cur_id, code, date, type, '0', value)


def new_term(spike):
    new_one = Term()
    new_one.code = spike.code
    new_one.name = '%s_term_%s_%s' % (spike.code, spike.type, spike.date)
    new_one.type = spike.type
    new_one.direct = 'unknown'
    new_one.append(spike)
    return new_one


def avg(cur, hist, dur):
    total = cur.col4
    for i in range(1, dur):
        total += hist[-i].col4
    return float(total / dur)


def create_or_update_term(terms, spike):
    term = terms[-1]
    # if spike is not None and spike.date == '2021-09-22':
    #     print spike.date
    if term.direct == 'unknown':
        term.append(spike)
        if term.direct == 'unknown':
            return None
        else:
            return term
    elif term.direct != '0' and int(term.direct) * (spike.value - term.spikes[-2].value) > 0:
        term.append(spike)
        return None
    elif term.direct == '0' and (term.spikes[-1].value - term.spikes[-3].value) * (spike.value - term.spikes[-2].value) < 0:
        term.append(spike)
        return None
    else:
        new_one = new_term(term.spikes[-2])
        new_one.append(term.spikes[-1])
        new_one.append(spike)
        terms.append(new_one)
        return new_one
