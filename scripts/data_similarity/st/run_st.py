# coding:utf-8

import os
from scripts.data_similarity.st.spike_terms.st_abstract import *
from scripts.data_similarity.st.spike_terms.st_obj1 import *
from scripts.data_similarity.st.spike_terms.st_subtest import *
from scripts.data_similarity.st.spike_terms.st_demo import *

if __name__ == '__main__':
    path = '../../../data/data_similarity/'
    name_filter = '.csv'
    # st = StAbstract(path)
    # st = StObj1(path)
    # st = StDemo(path)
    st = StSubTest(path)

    start = '2010-08-31'
    end = '2022-07-30'

    if not os.path.exists(path):
        print 'data path not exists'
        exit(255)

    if not os.path.isdir(path):
        print('data path is not a directory')
        exit(255)

    file_list = os.listdir(path)
    for f in file_list:
        if f.title().lower()[-4:] != name_filter:
            continue
        code = f.split('.')[0]
        st.go_through(code, path + f, [5, 20, start, end])
