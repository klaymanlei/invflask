#coding:utf-8

# 获取节假日及周末列表

import urllib2
import json
import time

api_url = r'https://api.apihubs.cn/holiday/get'

# api参数，仅获取节假日列表
api_params_holidays = r'month=%s&holiday_recess=1&field=date,weekend,holiday_recess'

# api参数，获取全部日期
api_params = r'month=%s&field=date,weekend,holiday_recess'


# 发送get请求
def get(url, params):
    try:
        req_url = params is None and url or r'%s?%s' % (url, params)
        # print req_url
        resp = urllib2.urlopen(req_url, timeout=120)
        res_str = resp.read()
        resp.close()
        return res_str
    except Exception, e:
        print str(e)
        return None


if __name__ == '__main__':
    start = '201501'
    end = '202212'
    current = start

    # 循环请求start到end之间的每个月数据
    while current <= end:
        report_json = json.loads(get(api_url, api_params % current))

        # 打印从接口读取的信息
        for day in report_json['data']['list']:
            print day['date'], \
                day['weekend'] == 2 and 'n' or 'y', \
                day['holiday_recess'] == 1 and 'y' or 'n'
        time.sleep(5)

        # 取下个月
        year = int(current[:4])
        month = int(current[4:])
        if month >= 12:
            month = 1
            year += 1
        else:
            month += 1
        current = r'%d%02d' % (year, month)
