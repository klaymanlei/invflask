#coding:utf-8
import datetime
import time

import random


def load(file):
    rs = []
    for f in open(file, 'r'):
        data = f.split(',')
        cur = float(data[2])
        date = data[1]
        rs.append((date, cur))
    rs.reverse()
    return rs


# 接受正序数列
def find_spikes(data):
    data.reverse()
    last_2 = ()
    last = ()
    cur = -1.0
    date = ''
    spikes = []
    for rec in data:
        if rec[1] == cur or rec[1] == 0:
            continue
        last_2 = last
        last = (date, cur)
        cur = rec[1]
        date = rec[0]
        # if len(spikes) == 0:
        #     spikes.append((date, cur))
        #     continue
        if len(last_2) < 2 or last_2[1] < 0 or last[1] < 0 or cur < 0:
            continue
        if (last[1] > cur and last[1] > last_2[1]) or (last[1] < cur and last[1] < last_2[1]):
            spikes.append(last)
    spikes.append(((date, cur)))
    spikes.reverse()
    return spikes


def split_part(spikes):
    parts = []
    temp_list = []
    direct = 0
    for spike in spikes:
        # if spike[0] == '2016/2/3':
        #     print spike
        temp_list.append(spike)
        if len(temp_list) < 4:
            continue
        elif len(temp_list) == 4:
            last_direct = temp_list[-2][1] - temp_list[-4][1]
            cur_direct = temp_list[-1][1] - temp_list[-3][1]
            if last_direct * cur_direct <= 0:
                direct = 0
            elif last_direct > 0 and cur_direct > 0:
                direct = 1
            else:
                direct = -1
        else:
            last_direct = temp_list[-2][1] - temp_list[-4][1]
            cur_direct = temp_list[-1][1] - temp_list[-3][1]
            if direct == 0:
                if last_direct * cur_direct <= 0:
                    continue
                else:
                    sub1 = []
                    sub1.extend(temp_list[:-1])
                    sub2 = temp_list[-3:]
                    parts.append((direct, sub1))
                    temp_list = sub2
            else:
                if last_direct * cur_direct > 0:
                    continue
                else:
                    sub1 = []
                    sub1.extend(temp_list[:-1])
                    sub2 = temp_list[-3:]
                    parts.append((direct, sub1))
                    temp_list = sub2
    parts.append((direct, temp_list))
    return parts


def merge_part(last, cur):
    list = last[1]
    for ele in cur[1]:
        if ele in list:
            continue
        list.append(ele)


# 接受正序数列
def gen_mn(data, n):
    temp = []
    rs = []
    for rec in data:
        if rec[1] == 0:
            continue
        temp.append(rec)
        if len(temp) < n:
            continue
        else:
            avg = 0
            for v in temp:
                avg += v[1]
            rs.append((temp[-1][0], round(float(avg / n), 2)))
            temp.pop(0)
    return rs


# 最小波动幅度
def diff_filter(data, threhold):
    rs = []
    for d in data:
        if len(rs) > 0 and float(d[1] / rs[-1][1]) > (1 - threhold) and  float(d[1] / rs[-1][1]) < (1 + threhold):
            continue
        if d[1] > 0:
            rs.append(d)
    return rs


# 最新值相对位置
def diff_filter2(data):
    rs = []
    for d in data:
        if len(rs) > 1 and float(rs[-2][1] - d[1]) * float(d[1] - rs[-1][1]) >= 0:
            continue
        if d[1] > 0:
            rs.append(d)
    return rs


def week_merge(data):
    rs = []
    week_no = None
    last = None
    for d in data:
        wk = datetime.datetime.strptime(d[0], '%Y/%m/%d').strftime('%Y%U')
        if week_no is None:
            week_no = wk
        elif wk <> week_no and last is not None:
            week_no = wk
            rs.append(last)
        last = d
    return rs


if __name__ == '__main__':
    file = '../../data/data_similarity/stcxA.csv'
    threhold = 0.03
    duration = 1

    data = load(file)
    # data = week_merge(data)
    # data = diff_filter(data, threhold)
    data = diff_filter2(data)
    m5 = gen_mn(data, duration)

    spikes = find_spikes(m5)

    parts = split_part(spikes)

    for part in parts:
        # print part[0], len(part[1]), part[1]

        print part[0], len(part[1]), part[1][2], part[1][3], len(part[1]) > 4 and part[1][4] or 'None, None'

        # print part[1][2][0], part[1][2][1], part[0], len(part[1])

        # print 'term%d_%s' % (duration, part[1][2][0]), part[0], part[1][0][0], part[1][0][1]
        # print 'term%d_%s' % (duration, part[1][2][0]), part[0], part[1][1][0], part[1][1][1]
        # print 'term%d_%s' % (duration, part[1][2][0]), part[0], part[1][-2][0], part[1][-2][1]
        # print 'term%d_%s' % (duration, part[1][2][0]), part[0], part[1][-1][0], part[1][-1][1]

        # print 'term%d_%s' % (duration, part[1][2][0]), part[0], part[0] == 1 and part[1][3] or part[1][2]