# coding:utf-8

# pip install selenium

import os
from browsermobproxy import Server
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# https://blog.csdn.net/weixin_39876282/article/details/110909471
# https://finance.sina.com.cn/realstock/company/sh600000/nc.shtml
# base_url = 'http://quote.eastmoney.com/'
base_url = 'https://finance.sina.com.cn/'
# class ="zxj"
# quote3l_c
# brief_info_c

if __name__ == '__main__':

    server = Server('D:/dev/python/browsermob-proxy/bin/browsermob-proxy.bat')
    server.start()
    proxy = server.create_proxy()

    chrome_opt = webdriver.ChromeOptions()
    chrome_opt.add_argument('--headless')
    chrome_opt.add_argument('--disable-gpu')
    chrome_opt.add_argument('blink-settings=imagesEnabled=false')
    chrome_opt.add_argument('--proxy-server={0}'.format(proxy.proxy))
    chrome_opt.add_argument('--ignore-certificate-errors')
    chrome_opt.add_argument('--ignore-urlfetcher-cert-requests')

    driver = webdriver.Chrome(executable_path=(r'C:/Program Files/Google/Chrome/Application/chromedriver107.exe'), chrome_options=chrome_opt)
    driver.implicitly_wait(1)

    try:
        proxy.new_har('douyin', options={'captureHeaders': True, 'captureContent': True})
        # driver.get(base_url + 'kcb/688082.html')
        driver.get(base_url + 'realstock/company/sh600000/nc.shtml')
        handlesList = driver.window_handles
        print handlesList[0], len(handlesList)
        result = proxy.har
        for entry in result['log']['entries']:
            _url = entry['request']['url']
            resp = entry['response']['content']
            if 'hq.sinajs.cn' in _url and 'sh600000' in _url:
                print _url
                print '\t', resp['text']

        # start_time = time.time()
        # print start_time

        exit(0)

        # driver.find_element_by_id('kw').send_keys('selenium webdriver')
        # driver.find_element_by_id('su').click()
        # driver.save_screenshot('screen.png')

        # ele = driver.find_element_by_id('price')
        # detail = driver.find_element_by_id('hqDetails')
        # all = driver.find_element_by_id('trading')
        # print ele.text
        # print detail.text
        # print all.text

        # 通过打开新的标签页， 可以节省浏览器打开的时间，减少资源的浪费
        js = 'window.open("https://www.jianshu.com/p/4fef4142b33f");'
        driver.execute_script(js)

        handlesList = driver.window_handles
        driver.switch_to.window(handlesList[1])
        driver.close() # 关闭标签页
        handlesList = driver.window_handles
        driver.switch_to.window(handlesList[0])

        js = 'window.open("https://www.jianshu.com/p/4fef4142b33f");'
        driver.execute_script(js)

        handlesList = driver.window_handles
    finally:
        print '关闭浏览器'
        # 关闭浏览器
        driver.quit()
        proxy.close()
        server.stop()

    end_time = time.time()
    print end_time
